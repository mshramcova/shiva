SHELL = sh

prefix       = /Users/masha/Dropbox/mydust_cvode/cvode_inst
exec_prefix  = /Users/masha/Dropbox/mydust_cvode/cvode_inst
includedir   = /Users/masha/Dropbox/mydust_cvode/cvode_inst/include/
libdir       = /Users/masha/Dropbox/mydust_cvode/cvode_inst/lib

CXX = g++
CFLAGS   = -O3 -DNDEBUG
LIBS     =  -lm

#LDFLAGS = -Wl,-rpath,/Users/masha/Dropbox/mydust_cvode/cvode_inst/lib
LDFLAGS = -Wl,-headerpad_max_install_names
INCLUDES =  -I${includedir}
LIBRARIES = -lsundials_cvode -lsundials_nvecserial ${LIBS}

TARGET= run.out

SRCS = bin/kinetics.cpp\
bin/dust_evolition_pro.cpp\
bin/service_functions.cpp\
bin/memallocate.cpp\
bin/time_step.cpp\
bin/time_step_ts_carb.cpp\
bin/etime_step.cpp\
bin/general_initialize.cpp\
bin/aromatization.cpp\
bin/photodestruction.cpp\
bin/shattering.cpp\
bin/sputtering.cpp\
bin/read_initial.cpp\
bin/optics.cpp\
bin/output.cpp\
bin/i_interaction_rnih.cpp\
bin/temperature.cpp

OBJS =	$(SRCS:.cpp=.o)
all:	$(TARGET)

$(TARGET):	$(OBJS)
	$(CXX) $(CFLAGS) -o $(TARGET) $(OBJS) ${INCLUDES} -L${libdir} $(LIBRARIES) ${LDFLAGS}

clean:  
	find . -name \*.o* > list
	xargs rm < list
	rm list

