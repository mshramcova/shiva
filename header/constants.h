//
//  Developed by Maria Murga 
//

#define _constant_Munit     2.2e9
#define _constant_Ge        6.67e-8            // cm^3 g^-1 s^-2
#define _constant_FourPIG   8.3818e-7          // cm^3 g^-1 s^-2
#define _constant_pc2cm     3.08567758e18      // cm
#define _constant_kpc2cm    3.08567758e21      // cm
#define _constant_km2cm     1e5                // cm
#define _constant_cto_km2cm 1e7            // cm
#define _constant_Msun      1.98892e33         // g
#define _constant_k_B       1.380658e-16		// Boltzmann's constant, erg/K
#define _constant_m0        1.6726231e-24      // g, mass of prothon
#define _constant_yr2sec    31536000.0         //
#define _constant_Habing    1.6e-3             // Habing UV flux ergs /cm^2 /s
#define _constant_Lsun      3.827e33           // Solar luminosity
#define _constant_clight    3e10				// speed of light [cm/c]
#define _constant_hplanck   6.626e-27			// Planck constant [erg*s]
#define _constant_hplanck_eV   4.135e-15			// Planck constant eV*s

#define _constant_Rsun     69550000000        // cm
#define _constant_E0       4.6                //eV
#define _constant_NA		 6.02214129e23		// Avogadro number
#define _constant_zpah      1               //Charge of a PAH grain

#define _constant_ze2       14.39       //squared charge of an electron, [eV*A]
#define _constant_ze        4.8e-10          //charge of an electron in sgs`   c   v         what
#define _constant_aem       1.662e-24   //A.e.m, g
#define _constant_e0        1.          //permittivity of free space - dielectricheckaya postoyannaya v vakuume
#define _constant_a_bohr    0.529      //A, Bohr's radius
#define _constant_k0        1.4e16
#define _constant_boltz     8.617e-5    //eV/K Boltzman's constant
#define _constant_me         5.486e-4   //mass of electron
#define _constant_boltz_sgs 1.381e-16 //erg/K  Boltzman's constant in sgs
#define _constant_ebind     2.9  //eV
#define _constant_le        10.0  //AA


