//
//  Developed by Maria Murga 
//
#include "standlibs.h"
#include "service.h"

class devol
{
    
    
private:
    
    
    
public:
    
    devol()
    {
        
            cout<<"    ╔╗╔╦═══╦╗ ╔╗ ╔══╗"<<endl;
            cout<<"    ║║║║╔══╣║ ║║ ║╔╗║"<<endl;
            cout<<"    ║╚╝║╚══╣║ ║║ ║║║║"<<endl;
            cout<<"    ║╔╗║╔══╣║ ║║ ║║║║"<<endl;
            cout<<"    ║║║║╚══╣╚═╣╚═╣╚╝║"<<endl;
            cout<<"    ╚╝╚╩═══╩══╩══╩══╝"<<endl;
            cout<<"    ╔══╗╔╗╔╦══╦════╗"<<endl;
            cout<<"    ║╔╗╚╣║║║╔═╩═╗╔═╝"<<endl;
            cout<<"    ║║╚╗║║║║╚═╗ ║║"<<endl;
            cout<<"    ║║ ║║║║╠═╗║ ║║"<<endl;
            cout<<"    ║╚═╝║╚╝╠═╝║ ║║"<<endl;
            cout<<"    ╚═══╩══╩══╝ ╚╝"<<endl;
            cout<<"    ╔═══╦╗╔╦══╦╗ ╔╗╔╦════╦══╦══╦╗ ╔╗"<<endl;
            cout<<"    ║╔══╣║║║╔╗║║ ║║║╠═╗╔═╩╗╔╣╔╗║╚═╝║"<<endl;
            cout<<"    ║╚══╣║║║║║║║ ║║║║ ║║  ║║║║║║╔╗ ║"<<endl;
            cout<<"    ║╔══╣╚╝║║║║║ ║║║║ ║║  ║║║║║║║╚╗║"<<endl;
            cout<<"    ║╚══╬╗╔╣╚╝║╚═╣╚╝║ ║║ ╔╝╚╣╚╝║║ ║║"<<endl;
            cout<<"    ╚═══╝╚╝╚══╩══╩══╝ ╚╝ ╚══╩══╩╝ ╚╝"<<endl;
        
        
        cout<<"    █ █  ███  ████  ███  ███  ████  █  █   █     ████"<<endl;
        cout<<"    █ █  █    █  █  █     █   █  █  ██ █  ██     █  █"<<endl;
        cout<<"    █ █  ███  ████  ███   █   █  █  █ ██   █     █  █"<<endl;
        cout<<"    ███  █    █ █     █   █   █  █  █  █   █     █  █"<<endl;
        cout<<"     █   ███  █ █   ███  ███  ████  █  █   █  █  ████"<<endl<<endl<<endl<<endl;
          
            
    }
    
    
    int nd;                 // number of dust grains
    
    //  -------------------------------    Various time step stuff  -------------------------

    int FileNumber;         // current file number
    double T_max;           //  time of calculation
    double DT;              //
    double dt;              // current time step
    double dt_step;         //minimum time step for timescale calculations
    double T_out;           // current time of the file output
    double TNOW;            // current time
    double nH;
    double T;
    double fieldfactor;
    double ro;
    double ne;
    double chi_he;
    double chi_c;
    double vgas;
    double fi_h;
    double fi_he;
    double fi_c;
    double vdust;
    //  -------------------------------------------------------------------------------------
    
    
    //  -------------------------------    Coordinate (size, aromatiz) properties  ----------

    int Nbin_a;             // number of bins along grain size
    int Nbin_e;             // number of bins along grain aomatization
    
    double **vel_of_bin;
    double **vel_temp;
    double **dsp_of_bin;

    

    
    //  -------------------------------    Some dimenstion sizes  ---------------------------

    int nsize;
    int nsize_pah;
    int ngap;
    int nwave;
    int n_ini;
    int n_ini_charge;
    int nla;
    int nai;
    int lenE;
    int nt;
    int nstep;
    int nlafull;
    int nv;
    int nteta;
    int nbin_shat;
    int n_ini_vel;
    int n_ini_velgas;
    int n_dielec_per1;
    int n_dielec_per2;
    int n_dielec_par1;
    int n_dielec_par2;
    int n_udpde;
    int n_adpde;
    int n_edpde;
    int nsize_heat_pah;
    int nsize_heat;
    int nt_heat_pah;
    int disp;
    int gdcouple;
    int ts1;
    int ts2;
    int sd_output;
    char graintype[4];
    char charge_fname[100];
    char distr_fname[100];
    char flux_fname[100];
    char dmodel[100];
    char turbvel_fname[100];
    int arom_init;
    int incharge;    
    int photoar;
    int sputar;
    int photodestr;
    int sputterdestr;
    int shatterdestr;
    
    //  -------------------------------------------------------------------------------------
    

    
    
    //  -------------------------------    Dust density  -----------------------------------

    double **NNN;           // local current dust number density in the mesh (size;aroma)
    
    
    //  -------------------------------------------------------------------------------------

    //  ----------------    Matrixes for dust concentration Eq  -----------------------------
    
    double ******CCC;       //
    double ****LLL;         //
    double **BBB1;           //
    double **BBB1_neg;       //
    double **AAA1;           //
    double **AAA1_neg;       //
    double **AAA3;           //
    double **AAA3_neg;       //
    double **BBB3;           //
    double **BBB3_neg;       //
    double **AAA4;           //
    double **AAA4_neg;        //
    double **BBB4;           //
    double **BBB4_neg;        //
    double **SSS;           //
    double **SSS_neg;        //
    double **AAA2;           //
    double **AAA2_neg;       //
    double **BBB2;           //
    double **BBB2_neg;       //

    //  -------------------------------------------------------------------------------------
    
    
    
    
    //  -------------------------------    Optical parameters of grains  --------------------
    
    int *idx;

    double *m;
    double *a;
    double *E;
    double *F;
    double *eg;
    double *ai2;
    double *egb;
    double **nhb;
    double *ai;
    double *fieldflux;
    double *fieldflux_orig;
    double *la;
    double *mb;
    double *ab;
    double *a_ini;
    double *size;
    double *size_pah;
    double *size_heat;
    double *nc_heat_pah;
    double *lafull;
    double *abin;
    double *amod;
    double *distr;
    double *distr2;
    double *int_func;
    double *egfull;
    double *wave_opt;
    double *dnda_ini;
    double *charge_ini;
    double *charge;
    double *a_ini_charge;
    double *a_ini_vel;
    double *a_ini_velgas;
    double *vel_ini;
    double *disp_ini;
    double *Y_c2h2_int;
    double *udpde;
    double *adpde;
    double *e_dpde;
    double *wave1_dielec;
    double *remper1;
    double *imper1;
    double *wave2_dielec;
    double *remper2;
    double *imper2;
    double *wave3_dielec;
    double *rempar1;
    double *impar1;
    double *wave4_dielec;
    double *rempar2;
    double *impar2;

    double *weightm;
    double *ai_ro;
    double *nshat;
    double *ai_idx;
    double *nshat_idx;
    double *ai_ro_idx;

    double *nm;
    double *na;
    double *rn_fv;
    double *re_fv;
    double *re_h_fv;
    double *rn_h_fv;
    
    double *fvxrn;
    double *ve;
    double *fv;
    double *re_c_fv;
    double *teta;
    double *tteta;
    double *steta;
    double *cteta;
    double *sigma_g;
    double *for_int_rei_c;
    double *for_int_rei_h;
    double *rs;
    double *gamma2;

    double *for_int_re_c;
    double *for_int_re_h;

    double *it_seems_that_strange_v;
    double *it_seems_that_strange_s;
    
    double ****qabs_opt;
    double ***qabs_opt_pah;
    
    
    double ***tmod, **tmod_pah;
    double *emod, *emod_pah;
    double ***dpde0,***dpde1;
    
    double *timescale;
    double *etimescale;

    //  -------------------------------------------------------------
    
    
//  ----------------    External parameters of ISM  ----------------
    
    double **gas2dus_relative_veloc;
    
    //  ------------------------------------------------------------------------------------
    
    //  -------------------------------    Some constants  -----------------------------------

    double amin;
    double amax;
    
    double nc200;
    double eg200;
    double phi1;
    double mineg;
    
    //  ------------------------------------------------------------------------------------
    
#ifdef CVODE
    
    int cv_lmm;             //	ÏÂÚÓ‰ Â¯ÂÌËˇ ƒ”: 0 - ADAMS, 1 - BDF (0)
    int cv_iter;            //	ÚËÔ ËÚÂ‡ˆËÈ: 0 - FUNCTIONAL, 1 - NEWTON (‰Îˇ ÊÂÒÚÍÓÈ ÂÍÓÏÂÌ‰Ó‚‡Ì Õ¸˛ÚÓÌ)
    int cv_itol;            //	ÚËÔ Ó¯Ë·ÓÍ: 0 - SS - ÒÍ‡ÎˇÌ‡ˇ ÓÚÌÓÒËÚÂÎ¸Ì‡ˇ Ë ÒÍ‡ÎˇÌ‡ˇ ‡·ÒÓÎ˛ÚÌ‡ˇ,
    //              1 - SV - ÒÍ‡ÎˇÌ‡ˇ ÓÚÌÓÒËÚÂÎ¸Ì‡ˇ Ë ‚ÂÍÚÓÌ‡ˇ ‡·ÒÓÎ˛ÚÌ‡ˇ (2)
    
    double cv_reltol;       //	‚ÂÎË˜ËÌ‡ ÓÚÌÓÒËÚÂÎ¸ÌÓÈ Ó¯Ë·ÍË
    double cv_abstol;       //	‚ÂÎË˜ËÌ‡ ‡·ÒÓÎ˛ÚÌÓÈ Ó¯Ë·ÍË ()
    //	ewt[i] = 1/(reltol*abs(y[i]) + abstol)   (if itol = SS), or
    //  ewt[i] = 1/(reltol*abs(y[i]) + abstol[i])   (if itol = SV).
    bool cv_optIn;          //	”Í‡Á˚‚‡Ú¸ ‰ÓÔÓÎÌËÚÂÎ¸Ì˚Â Ô‡‡ÏÂÚ˚ ‚ iopt Ë ropt (0)
    
    int cv_itask;           //	0 - NORMAL - интерполировать до t_out и выводить приближенное значение y(t_out)
    //  1 - ONE_STEP - выводить значение в ближайшей просчитаной точке y(*t);
    
    
    
    long int *cv_iopt;
    double *cv_ropt;
    
    FILE* cvode_log;
    
    void *cvode_mem;
    int error_cvode;
    double *machEnv;
    
#endif
        
    //=========================================================================================
    //============= FUNCTIONS =======================
    //=========================================================================================
    
    
    
    void integrator(double dt);
    void ts_carb(double dt);
    void ts_hydr(double dt);
    double right_part(int i, int j, double dt_loc);

    void Aij(int I, int J);
    void Rij(int I, int J);
    void Sij(int I, int J);
    void calc_CCC(void);
    
    void memallocate(void);
    
    void read_initial_data(void);
    void read_initial_data2(void);

    void general_intialization();
    
    double optics_qabs(double l, double aa, double eg, double Z);
    double coulombian_factor(double charge1,double charge2,double a1,double a2,double m1,double m2,double vcol);
    double tvib_mol(double avib, double ncvib, double egvib, double epoint);

    
    //--------  aromatization functions -----------------------
    
    double eg_aroma(double eg_old, double aa, double Z);

    
    //--------  photodestruction functions -----------------------
    
    double Yion(double e, double Z, double aa);
    double kir(double aa, double nc, double E, double Z);
    double photodestruction_rate(double  aa, double Z, int ihere);
    double prob_mol(double ahere, double Z, double uhere, double ediss);

    
    //-------   sputtering functinos  ---------------------------
    
    void big_sput(double &in_bigsput_rate, double &in_bigsput_aroma_rate, double aa, double *vpah, double nH, double eg,  double z_charge);
    void big_sput_t(double &in_bigsput_rate_t, double &in_bigsput_aroma_rate_t,double aa, double nH, double T, double eg, double z_charge);

    double rate_sput(double aa, double v, double nH,  char iontype, double eg, double z_charge);
    double rsput_t(char iontype, double aa, double eg, double nH, double T, double z_charge);
    double Ysp(char iontype, double E, double eg);
    void sputter(double &U0, double &M2, double &Z2, double &K);
    //void thermal(double &rn_t, double &re_t, double &re_h_t,
    //             char iontype, double apah, double nH, double T, double eg);
    void small_sput(double &rate_stat_aroma, double &rate_nonstat_aroma, double &rate_stat, double &rate_nonstat,
                    double *vpah, double T, double nH, double egold, double aa, double z_charge);
    
    void i_interaction_ri(double &rni, double &rni_h, double &rei, double &rei_h,
                          char iontype, double apah, double Nc0,
                          double eg, double nH, double vpah,  double z_charge);
    void e_interaction_re(double & re_c_t, double & re_h_t, double Nc0, double apah, double nH, double T, double eg, double z_charge);
    
    void thermal(double & rn_t, double & rn_h_t, double & re_t, double & re_h_t,
                 char iontype,
                 double apah, double Nc0, double nH, double T, double eg,
                 double z_charge);
    void diss_rate(double &p_c, double &p_h, double &p_h2, double xh, double Nc0, double Efull, double apah, double z_charge);


    //--------  shattering functions    -----------------------------
    
    void mshat(double & mshat1, double & mshat2, double & mold,
                      double m1, double m2, double a1, double a2,
                      double v, int i_a, int i_eg, int i_subeg1, int i_subeg2);

    void set_idx(double *arr_in, int* arr_idx, int size, int &len, double a_left, double a_right);
    void init_arr_by_idx(double *arr, double *arr_in, int *arr_idx, int len);
    void total_output(void);
    void ts_output(void);
    void shattering_data(double &P1, double &c0, double &Pv, double &s_shat, double &v_shat);
    void density(double &ro);
    double n_ion(char iontype);
    double fi_ion(char iontype);
#ifdef CVODE

    bool KeyCVode;

    static int Jac(long int N, double t,
                   N_Vector y, N_Vector fy, DlsMat J, void *user_data,
                   N_Vector tmp1, N_Vector tmp2, N_Vector tmp3);
    
    static int RF(realtype t, N_Vector y, N_Vector ydot, void *user_data);
   
    int DustKinetics();
    N_Vector y0, y_out, y, abstol;
    
    static int check_flag(void *flagvalue, char *funcname, int opt);
    static void PrintOutput(realtype t, realtype y1, int i, int imax);
#define Ith(v,i)    NV_Ith_S(v,i)       /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) DENSE_ELEM(A,i,j) /* IJth numbers rows,cols 1..NEQ */

#endif

};
