//
//  Developed by Maria Murga
//

#ifndef _standlibs_h
#define _standlibs_h


#define CVODE

//#include <type_traits>
#include <cmath>
#include <ctime>
#include <string.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>
//#include <direct.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

using namespace std;


#include "constants.h"
#include "service.h"


#ifdef _parallel_staff_omp
#include <omp.h>
#endif


#ifdef CVODE
#include "../cvode/cvode.h"             /* prototypes for CVODE fcts., consts. */
#include "../cvode/nvector_serial.h"  /* serial N_Vector types, fcts., macros */
#include "../cvode/cvode_dense.h"      /* prototype for CVDense */
#endif


#endif
