//
//  Developed by Maria Murga 
//

#ifndef _service_h
#define _service_h

int *AllocateInt(const int &n);
int **AllocateInt(const int &n, const int &m);
int *** Allocate3dArrayInt(const int &n, const int &m, const int &l);

double *Allocate(const int &n);
char *Allocate_char(const int &n);

double **Allocate(const int &n, const int &m);
double ***Allocate(const int &n, const int &m, const int &l);
double **** Allocate(const int &n, const int &m, const int &l, const int &s);
double ****** Allocate(const int &n1, const int &n2, const int &n3, const int &n4, const int &n5, const int &n6);

void DeAllocate2dArray(double **a, const int &n, const int &m);
void DeAllocate3dArray(double ***a, const int &n, const int &m, const int &l);

void atomic_data(int &z1_ion, double &chi_ion, double &m1_ion, double &E0n_ion, double &E0n_ion_h, double &L_ion, double &r2_ion, int &z2_pah, int &zion, double &m2_pah,char iontype);

double func_mion(char iontype);

double planck_lambda(double la, double T);


double min(double *a, int len_a);
double max(double *a, int len_a);

void logscale(double *p, double minvalue, double maxvalue, int nstep);
double interp_1d(double x, double *x1, double *y1, int n);
double trapz(double *arr_x, double *arr_y, int len_arr_x, int n);


void Write1dArrToFile_int(ofstream &out, const int &n, int *&Array);
void Write1dArrToFile_float(ofstream &out, const int &n, double *&Array);
void Write1dArrToFile_double(ofstream &out, const int &n, double *&Array);

void Write3dArrToFile_float(ofstream &out, const int &n, const int &m, const int &l,  double ***&Array);
void Write4dArrToFile_double(ofstream &out, const int &n, const int &m, const int &l, const int &x, double ****&Array);
void Write3dArrToFile_double(ofstream &out, const int &n, const int &m, const int &l, double ***&Array);
void Write4dArrToFile_float(ofstream &out, const int &n, const int &m, const int &l, int o, double ****&Array, double ***&rho);

void ReadArrFromFile1d_double(ifstream &in, const int &n, double *&Array);
void ReadArrFromFile1d_float(ifstream &in, const int &n, double *&Array);
void ReadArrFromFile1d_int(ifstream &in, const int &n, int *&Array);

void ReadArrFromFile2d_double(ifstream &in, const int &n, const int &m, double **&Array);
void ReadArrFromFile3d_double(ifstream &in, const int &n, const int &m, const int &l, double ***&Array);
void ReadArrFromFile4d_double(ifstream &in, const int &n, const int &m, const int &l, const int &p, double ****&Array);

#endif
