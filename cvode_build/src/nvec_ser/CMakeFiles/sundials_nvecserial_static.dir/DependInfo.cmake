# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/src/sundials/sundials_math.c" "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/nvec_ser/CMakeFiles/sundials_nvecserial_static.dir/__/sundials/sundials_math.c.o"
  "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/src/nvec_ser/nvector_serial.c" "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/nvec_ser/CMakeFiles/sundials_nvecserial_static.dir/nvector_serial.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_SUNDIALS_LIBRARY"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include"
  "include"
  "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/src/nvec_ser/."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )
