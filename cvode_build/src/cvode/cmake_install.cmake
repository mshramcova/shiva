# Install script for directory: /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/src/cvode

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/masha/Dropbox/mydust_cvode/cvode_inst")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  MESSAGE("
Install CVODE
")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/cvode/libsundials_cvode.a")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libsundials_cvode.a" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libsundials_cvode.a")
    execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libsundials_cvode.a")
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/cvode/libsundials_cvode.1.0.0.dylib"
    "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/cvode/libsundials_cvode.1.dylib"
    "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/cvode/libsundials_cvode.dylib"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libsundials_cvode.1.0.0.dylib"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libsundials_cvode.1.dylib"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libsundials_cvode.dylib"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      execute_process(COMMAND "/opt/local/bin/install_name_tool"
        -id "libsundials_cvode.1.dylib"
        "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cvode" TYPE FILE FILES
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_band.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_bandpre.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_bbdpre.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_dense.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_diag.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_direct.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_sparse.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_spbcgs.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_spgmr.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_spils.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include/cvode/cvode_sptfqmr.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cvode" TYPE FILE FILES "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/src/cvode/cvode_impl.h")
endif()

