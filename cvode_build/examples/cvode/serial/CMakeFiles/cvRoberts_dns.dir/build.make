# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.3

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/local/bin/cmake

# The command to remove a file.
RM = /opt/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/masha/Dropbox/mydust_cvode/cvode_build

# Include any dependencies generated for this target.
include examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/depend.make

# Include the progress variables for this target.
include examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/progress.make

# Include the compile flags for this target's objects.
include examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/flags.make

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/flags.make
examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o: /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/cvode/serial/cvRoberts_dns.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/Users/masha/Dropbox/mydust_cvode/cvode_build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o"
	cd /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o   -c /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/cvode/serial/cvRoberts_dns.c

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.i"
	cd /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/cvode/serial/cvRoberts_dns.c > CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.i

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.s"
	cd /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/cvode/serial/cvRoberts_dns.c -o CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.s

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.requires:

.PHONY : examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.requires

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.provides: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.requires
	$(MAKE) -f examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/build.make examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.provides.build
.PHONY : examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.provides

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.provides.build: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o


# Object files for target cvRoberts_dns
cvRoberts_dns_OBJECTS = \
"CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o"

# External object files for target cvRoberts_dns
cvRoberts_dns_EXTERNAL_OBJECTS =

examples/cvode/serial/cvRoberts_dns: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o
examples/cvode/serial/cvRoberts_dns: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/build.make
examples/cvode/serial/cvRoberts_dns: src/cvode/libsundials_cvode.1.0.0.dylib
examples/cvode/serial/cvRoberts_dns: src/nvec_ser/libsundials_nvecserial.0.0.2.dylib
examples/cvode/serial/cvRoberts_dns: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/Users/masha/Dropbox/mydust_cvode/cvode_build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable cvRoberts_dns"
	cd /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/cvRoberts_dns.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/build: examples/cvode/serial/cvRoberts_dns

.PHONY : examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/build

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/requires: examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o.requires

.PHONY : examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/requires

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/clean:
	cd /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial && $(CMAKE_COMMAND) -P CMakeFiles/cvRoberts_dns.dir/cmake_clean.cmake
.PHONY : examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/clean

examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/depend:
	cd /Users/masha/Dropbox/mydust_cvode/cvode_build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2 /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/cvode/serial /Users/masha/Dropbox/mydust_cvode/cvode_build /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial /Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/depend

