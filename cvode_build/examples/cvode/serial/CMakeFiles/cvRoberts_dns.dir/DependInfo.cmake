# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/cvode/serial/cvRoberts_dns.c" "/Users/masha/Dropbox/mydust_cvode/cvode_build/examples/cvode/serial/CMakeFiles/cvRoberts_dns.dir/cvRoberts_dns.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/include"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/cvode/CMakeFiles/sundials_cvode_shared.dir/DependInfo.cmake"
  "/Users/masha/Dropbox/mydust_cvode/cvode_build/src/nvec_ser/CMakeFiles/sundials_nvecserial_shared.dir/DependInfo.cmake"
  )
