# Install script for directory: /Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/nvector/serial

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/Users/masha/Dropbox/mydust_cvode/cvode_inst")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial/test_nvector_serial.c;/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial/test_nvector.c;/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial/test_nvector.h;/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial/sundials_nvector.c")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial" TYPE FILE FILES
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/nvector/serial/test_nvector_serial.c"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/nvector/serial/../test_nvector.c"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/nvector/serial/../test_nvector.h"
    "/Users/masha/Dropbox/mydust_cvode/cvode-2.8.2/examples/nvector/serial/../../../src/sundials/sundials_nvector.c"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial/CMakeLists.txt")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial" TYPE FILE FILES "/Users/masha/Dropbox/mydust_cvode/cvode_build/examples/nvector/serial/CMakeLists.txt")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial/Makefile")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/masha/Dropbox/mydust_cvode/cvode_inst/examples/nvector/serial" TYPE FILE RENAME "Makefile" FILES "/Users/masha/Dropbox/mydust_cvode/cvode_build/examples/nvector/serial/Makefile_ex")
endif()

