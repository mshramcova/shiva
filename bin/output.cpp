//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

void Write1dArrToFile(ofstream &out, const int &n, double *&Array)
{
    
    for(int i = 0; i < n; i++)
        out<<Array[i]<<endl;//    out.write((char*)(&Array[i]),sizeof(double));
    
}

void Write2dArrToFile(ofstream &out, const int &n, const int &m, double **&Array)
{
    
    for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
        out<<Array[i][j]<<endl;//out.write((char*)(&Array[i][j]),sizeof(double));
    
}

void devol::total_output(void)
{
    
    int stat = mkdir("_dat",S_IRWXU);

    char FileName[150];
    
    
    ofstream file_out;
    
    if(TNOW==0)
    {
      FileNumber=0;
      remove("_dat/result.dat");
    }
//return;    
    sprintf(FileName, "_dat/result.dat");
    
    cout<<"Output coming... "<<FileName<<" "<<FileNumber<<endl;
    
    file_out.open(FileName,ios::app);
    file_out.setf(ios::scientific);

    for (int i_eg=0; i_eg <Nbin_e; i_eg++)
    {
        file_out<<i_eg<<" "<<FileNumber<<endl;
        
        for (int i_size=0; i_size<Nbin_a; i_size++)
        {
            file_out<<" "<<a[i_size]<<" "<<eg[i_eg]<<" "<<" "<<NNN[i_size][i_eg]<<endl;
        }
    }
    
    file_out.close();
        
    FileNumber++;
}


void devol::ts_output(void)
{
    
    int stat = mkdir("_dat",S_IRWXU);

    char FileName_rate[150], FileName_rate2[150];
        
    ofstream file_out_rate,file_out_rate2;
    
    remove("_dat/result_ts.dat");
    remove("_dat/result_ts_hydr.dat");
    sprintf(FileName_rate, "_dat/result_ts.dat");
    sprintf(FileName_rate2, "_dat/result_ts_hydr.dat");
    
    cout<<"Output coming... "<<FileName_rate<<endl;
    
    file_out_rate.open(FileName_rate,ios::app);
    file_out_rate.setf(ios::scientific);
    file_out_rate2.open(FileName_rate2,ios::app);
    file_out_rate2.setf(ios::scientific);
    file_out_rate<<"    a, cm       destr_timescale, yr"<<endl;
    file_out_rate2<<"    a, cm       arom_timescale, yr"<<endl;
    for (int i_size=0; i_size<Nbin_a; i_size++){
            file_out_rate<<" "<<a[i_size]<<" "<<" "<<timescale[i_size]<<endl;
    }
    for (int i_size=0; i_size<Nbin_a; i_size++){
            file_out_rate2<<" "<<a[i_size]<<" "<<" "<<etimescale[i_size]<<endl;
    }
    
    file_out_rate.close();
    file_out_rate2.close();

}
