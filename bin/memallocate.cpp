//
//  Developed by Maria Murga 
//
#include "../header/dust_evolition_pro.h"

void devol::memallocate(void)
{
    
    NNN      = Allocate(Nbin_a,Nbin_e);
        
    AAA1      = Allocate(Nbin_a,Nbin_e);
    AAA1_neg  = Allocate(Nbin_a,Nbin_e);
    BBB4      = Allocate(Nbin_a,Nbin_e);
    BBB4_neg  = Allocate(Nbin_a,Nbin_e);
    AAA4      = Allocate(Nbin_a,Nbin_e);
    AAA4_neg  = Allocate(Nbin_a,Nbin_e);
    CCC      = Allocate(Nbin_a,Nbin_e,Nbin_a,Nbin_e,Nbin_a,Nbin_e);
    LLL      = Allocate(Nbin_a,Nbin_e,Nbin_a,Nbin_e);
    AAA2      = Allocate(Nbin_a,Nbin_e);
    AAA2_neg  = Allocate(Nbin_a,Nbin_e);
    BBB2      = Allocate(Nbin_a,Nbin_e);
    BBB2_neg  = Allocate(Nbin_a,Nbin_e);
    BBB1      = Allocate(Nbin_a,Nbin_e);
    BBB1_neg  = Allocate(Nbin_a,Nbin_e);
    SSS      = Allocate(Nbin_a,Nbin_e);
    SSS_neg  = Allocate(Nbin_a,Nbin_e);
    BBB3      = Allocate(Nbin_a,Nbin_e);
    BBB3_neg  = Allocate(Nbin_a,Nbin_e);
    AAA3      = Allocate(Nbin_a,Nbin_e);
    AAA3_neg  = Allocate(Nbin_a,Nbin_e);
    
    a = Allocate(Nbin_a);
    m = Allocate(Nbin_a);
    E = Allocate(lenE);
    F = Allocate(lenE);
    eg = Allocate(Nbin_e);
    ai = Allocate(nai);
    ab = Allocate(Nbin_a+1);
    mb = Allocate(Nbin_a+1);
    la =  Allocate(nla);
    egb =  Allocate(Nbin_e+1);
    nhb = Allocate(Nbin_a+1,Nbin_e+1);
    size = Allocate(nsize);
    size_pah = Allocate(nsize_pah);
    a_ini = Allocate(n_ini);
    int_func = Allocate(nlafull);
    amod = Allocate(nstep);
    egfull = Allocate(ngap);
    abin = Allocate(nstep);
    distr = Allocate(nstep);
    
    ai = Allocate(nai);
    ai2 = Allocate(nbin_shat);
	ai_ro = Allocate(nbin_shat);
	nshat = Allocate(nbin_shat);
	idx = AllocateInt(nbin_shat);
	ai_idx = Allocate(nbin_shat);
	nshat_idx = Allocate(nbin_shat);
	ai_ro_idx = Allocate(nbin_shat);
    a_ini_charge = Allocate(n_ini);
    charge_ini  = Allocate(n_ini);
    a_ini_vel = Allocate(n_ini_vel);
    vel_ini  = Allocate(n_ini_vel);
    disp_ini = Allocate(n_ini_vel);
    charge = Allocate(Nbin_a);
    distr2 = Allocate(nstep);
    weightm = Allocate(nstep);
    nm = Allocate(Nbin_a);
    na = Allocate(Nbin_a);
    udpde = Allocate(n_udpde);
    adpde = Allocate(n_adpde);
    e_dpde = Allocate(n_edpde);
    dpde1 = Allocate(n_adpde, n_udpde, n_edpde);
    dpde0 = Allocate(n_adpde, n_udpde, n_edpde);
    lafull = Allocate(nlafull);
    
    Y_c2h2_int = Allocate(nla);
    dnda_ini  = Allocate(n_ini);
    wave_opt  = Allocate(nwave);
    fieldflux_orig = Allocate(nla);
    fieldflux = Allocate(nlafull);
    
    tmod = Allocate(nsize,ngap,nt);
    tmod_pah = Allocate(nsize_heat_pah, nt_heat_pah);
    qabs_opt = Allocate(2,nsize,ngap,nwave);
    qabs_opt_pah = Allocate(2,nsize_pah,nwave);
    
    emod = Allocate(nt);
    emod_pah = Allocate(nt_heat_pah);
    size_heat = Allocate(8);
    nc_heat_pah = Allocate(nsize_heat_pah);

    wave1_dielec = Allocate(n_dielec_per1);
    remper1 = Allocate(n_dielec_per1);
    imper1 = Allocate(n_dielec_per1);
    wave2_dielec = Allocate(n_dielec_per2);
    remper2 = Allocate(n_dielec_per2);
    imper2 = Allocate(n_dielec_per2);
    wave3_dielec = Allocate(n_dielec_par1);
    rempar1 = Allocate(n_dielec_par1);
    impar1 = Allocate(n_dielec_par1);
    wave4_dielec = Allocate(n_dielec_par2);
    rempar2 = Allocate(n_dielec_par2);
    impar2 = Allocate(n_dielec_par2);
    
    vel_of_bin = Allocate(Nbin_a, Nbin_e);
    vel_temp = Allocate(Nbin_a, Nbin_e);
    dsp_of_bin = Allocate(Nbin_a, Nbin_e);
    gas2dus_relative_veloc = Allocate(Nbin_a,3);
    
    it_seems_that_strange_v = Allocate(nv);

    teta = Allocate(nteta);
    tteta = Allocate(nteta);
    steta = Allocate(nteta);
    cteta = Allocate(nteta);
    sigma_g = Allocate(nteta);
    rs = Allocate(nteta);
    gamma2 = Allocate(nteta);
    it_seems_that_strange_s = Allocate(nteta);

    for_int_rei_c = Allocate(nteta);
    for_int_rei_h = Allocate(nteta);
    for_int_re_c = Allocate(nteta);
    for_int_re_h = Allocate(nteta);
    
    rn_fv = Allocate(nv);
    rn_h_fv = Allocate(nv);
    re_fv = Allocate(nv);
    re_h_fv = Allocate(nv);
    fvxrn  = Allocate(nv);
    
    ve = Allocate(nv);
    fv = Allocate(nv);
    re_c_fv = Allocate(nv);
    re_h_fv = Allocate(nv);
    
    timescale = Allocate(Nbin_a);
    etimescale = Allocate(Nbin_a);

}
