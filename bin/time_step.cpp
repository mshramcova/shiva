//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

void devol::integrator(double dt_pointer)
{
    dt = dt_pointer;
    if (shatterdestr==1){
    calc_CCC();// + shattering
    }
    cout<<"shat done "<<endl;
    for (int i=0; i< Nbin_a; i++)
    for (int j=0; j< Nbin_e; j++)                                                                   
    {
        if (photoar==1){
        Aij(i,j);   // + aromatization
        }
        if (photodestr==1){
        Rij(i,j);   // + photodestruction
        }
        if ((sputar==1)|(sputterdestr ==1)){
        Sij(i,j);   // + sputtering
        }
    }
    cvode_mem = NULL;
    y = abstol = NULL;

    y = N_VNew_Serial(Nbin_a*Nbin_e);
    abstol = N_VNew_Serial(Nbin_a*Nbin_e);

    int o=0;
    for (int i=0; i< Nbin_a; i++)
    for (int j=0; j< Nbin_e; j++)
    {
        Ith(y, o) = NNN[i][j];
        o++;
    }
    
    int flag, flagr, iout;
    
    realtype tloc(0.0), t, tout;
    double dtk = dt * _constant_yr2sec;
    
    cv_reltol = 1e-15;          //	No smaller than 1e-15
    
    o = 0;
    for (int i=0; i<Nbin_a; i++)
    {
        for (int j=0; j<Nbin_e; j++)
        {
            
            Ith(abstol, o) = 1.0e-15*pow(10,-i);
            o++;
        }
    }
    /* Call CVodeCreate to create the solver memory and specify the
     * Backward Differentiation Formula and the use of a Newton iteration */
    cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
    if (check_flag((void *)cvode_mem, "CVodeCreate", 0)) cin.get();

    /* Call CVodeInit to initialize the integrator memory and specify the
     * user's right hand side function in y'=f(t,y), the inital time T0, and
     * the initial dependent variable vector y. */
    flag = CVodeInit(cvode_mem, RF, tloc, y);
    if (check_flag(&flag, "CVodeInit", 1)) cin.get();
    
    /* Call CVodeSVtolerances to specify the scalar relative tolerance
     * and vector absolute tolerances */
    flag = CVodeSVtolerances(cvode_mem, cv_reltol, abstol);
    if (check_flag(&flag, "CVodeSVtolerances", 1)) cin.get();
    
    /* Call CVDense to specify the CVDENSE dense linear solver */
    flag = CVDense(cvode_mem, Nbin_e*Nbin_a);
    if (check_flag(&flag, "CVDense", 1)) cin.get();
    
    /* Set the Jacobian routine to Jac (user-supplied) */
    //flag = CVDlsSetDenseJacFn(cvode_mem, Jac);
    //if (check_flag(&flag, "CVDlsSetDenseJacFn", 1)) cin.get();
    
    flag = CVodeSetUserData(cvode_mem, (void*)this);
    if (check_flag(&flag, "CVodeSetUserData", 1)) cin.get();
    
    flag = CVodeSetMaxNumSteps(cvode_mem, 3000000);
    if (check_flag(&flag, "CVodeSetMaxNumSteps", 1)) cin.get();
    /* In loop, call CVode, print results, and test for error.
     Break out of loop when NOUT preset output times have been reached.  */
    iout = 0;  tout = dtk;
    int imax = int(T_max/dt);
    int nprint = int(T_max/DT);
    int iprint = imax/nprint; 
    while(1) {
        
        //cout<<"tout = "<<tout<<endl;
        flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
        if (check_flag(&flag, "CVode", 1)) break;
        if (flag == CV_SUCCESS) {
            iout++;
            tout += dtk;
        }
        TNOW = tout;
        o=0;
        for (int i=0; i< Nbin_a; i++)
            for (int j=0; j< Nbin_e; j++)
            {
                if (Ith(y,o)<0.){
                    Ith(y,o)=0.;
                }
                NNN[i][j] = Ith(y,o);
                
                o++;
            }
        /*if (iout%(iprint/10) ==0)
        {
            cout<<"1/10 of one step is done"<<endl;
        }*/
        if (iout%iprint ==0)
        {
            PrintOutput(t/_constant_yr2sec,T_max, iout, imax);
            total_output();
        }
        if (iout >= imax) break;
    }
    
    return;
}

int devol::check_flag(void *flagvalue, char *funcname, int opt)
{
    int *errflag;
    
    /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
    if (opt == 0 && flagvalue == NULL) {
        fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
                funcname);
        return(1); }
    
    /* Check if flag < 0 */
    else if (opt == 1) {
        errflag = (int *) flagvalue;
        if (*errflag < 0) {
            fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
                    funcname, *errflag);
            return(1); }}
    
    /* Check if function returned NULL pointer - no memory allocated */
    else if (opt == 2 && flagvalue == NULL) {
        fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
                funcname);
        return(1); }
    
    return(0);
}

void devol::PrintOutput(realtype t, realtype tmax, int i, int imax)
{
#if defined(SUNDIALS_EXTENDED_PRECISION)
    printf("t = %6.4e      tmax = %6.4Le  i = %5d/%5d \n", t, tmax, i,imax);
#elif defined(SUNDIALS_DOUBLE_PRECISION)
    printf("t = %6.4e      tmax = %6.4e  i = %5d/%5d \n", t, tmax, i, imax);
#else
    printf("t = %6.4e     tmax = %6.4e  i = %5d/%5d \n", t, tmax, i, imax);
#endif
    
    return;
}
