//
//  Developed by Maria Murga 
//
#include "../header/dust_evolition_pro.h"

void devol::general_intialization()
{
    int n = 0;
    double weight;
    double amed, xhb;
    amin = 3e-8;//min(a_ini, n_ini);
    amax = max(a_ini, n_ini);
    mineg = min(egfull,ngap);
    logscale(lafull, 900e-8, 999e-4, nlafull);
    for(int i=0; i<nlafull; i++){
        fieldflux[i] = interp_1d(lafull[i], la, fieldflux_orig, nla)*fieldfactor;
    }
    logscale(ab,amin, amax, Nbin_a+1); //borders of bins
    density(ro);
    for (int i=0; i<=Nbin_a; i++)
    {    mb[i] = 4./3.*M_PI*ro*pow(ab[i],3);
    }
    
    for (int i=0; i<=Nbin_a; i++)
    for (int j=0; j<=Nbin_e; j++){
            egb[j] = mineg + double(j)*(max(egfull,ngap)-mineg)/double(Nbin_e);
            xhb = egb[j]/4.3;
            amed = 12-11*xhb;
            nhb[i][j] = 4./3.*M_PI*ro*pow(ab[i],3)*_constant_NA/amed*xhb;
    }
    for (int i=0; i<Nbin_e; i++)
        eg[i] = 0.5*(egb[i]+egb[i+1]);
        
    for (int i=0; i<Nbin_a; i++)
    {
        a[i] = (ab[i]+ab[i+1])/2.;
        m[i] = 4./3.*M_PI*ro*pow(a[i],3);
    }
     
    
    for (int i=0; i<nteta; i++)
	{
 		teta[i] = 0.5*i*M_PI/(nteta-1);                   //angle of projectile
        tteta[i] = tan(teta[i]);                          //angle of projectile
        steta[i] = sin(teta[i]);                          //angle of projectile
        cteta[i] = cos(teta[i]);;                         //angle of projectile

	}

    
    
//  --------------------- INITIAL NUMBER DENSITY   --------
    if ((strcmp(graintype,"gra")==0)|(strcmp(graintype,"sil")==0)){
        arom_init=1;
    }
    
    for (int i=0; i<Nbin_a; i++)
    {
        na[i] = interp_1d(a[i],a_ini,dnda_ini,n_ini);
    
        nm[i] = 0.25 * na[i]/(M_PI*ro*a[i]*a[i]);
        
        if (arom_init==0){
        NNN[i][Nbin_e-1] = nm[i]*(mb[i+1]-mb[i])*nH;
        for (int jeg=0; jeg<Nbin_e-1; jeg++)
        {
            NNN[i][jeg] = 0.0;
        
         }}
        else if (arom_init==1){
        NNN[i][0] = nm[i]*(mb[i+1]-mb[i])*nH;
        for (int jeg=1; jeg<Nbin_e; jeg++)
        {
            NNN[i][jeg] = 0.0;
            
        }}
     cout<<"i = "<< i << " NNN = "<<NNN[i][0]<<" "<<NNN[i][1]<<" "<<NNN[i][2]<<" "<<NNN[i][3]<<" "<<NNN[i][Nbin_e-1]<<endl;
        cout<<endl;
    }
    cout<<"include charge = "<<incharge<<endl;
    cout<<endl;
    cout<<"a[i]         m[i]        z[i]   "<<endl;
    for (int i=0; i<Nbin_a; i++)
    {        
        if (incharge==1) {
            charge[i] = interp_1d(a[i],a_ini_charge, charge_ini, n_ini_charge);
        }
        else{
            charge[i] = 0.;}
        cout<<a[i]<<" "<<m[i]<<" "<<charge[i]<<" "<<endl;
        
    }

    //cout<<"a[i]         m[i]        vturb[i]   "<<endl;
    for (int i=0; i<Nbin_a; i++)
    for (int j=0; j<Nbin_e; j++)
    {
        if (gdcouple==0) {
            vel_temp[i][j] = sqrt(8*_constant_k_B*T/M_PI/m[i]);
            vel_of_bin[i][j] = sqrt(pow(interp_1d(a[i],a_ini_vel, vel_ini, n_ini_vel),2)+pow(vel_temp[i][j],2));
        }
        else {
            vel_of_bin[i][j] = vdust;
            disp = 0;
        }
        
        if (disp==0){
            dsp_of_bin[i][j] = vel_of_bin[i][j];}
        else
            dsp_of_bin[i][j] = interp_1d(a[i],a_ini_vel, disp_ini, n_ini_vel);
    }
    for (int i=0; i<Nbin_a; i++)
    for (int j=0; j<Nbin_e; j++){
        /*gas2dus_relative_veloc[i][0] = sqrt(pow(vgas - vel_of_bin[i][j],2)+ pow(dsp_of_bin[i][j],2));
        gas2dus_relative_veloc[i][1]= sqrt(pow(vgas*sqrt(1./4.) - vel_of_bin[i][j],2)+ pow(dsp_of_bin[i][j],2));
        gas2dus_relative_veloc[i][2]= sqrt(pow(vgas*sqrt(1./12.) - vel_of_bin[i][j],2)+ pow(dsp_of_bin[i][j],2));*/
        gas2dus_relative_veloc[i][0] = vgas;
        gas2dus_relative_veloc[i][1]= vgas*sqrt(1./4.);
        gas2dus_relative_veloc[i][2]= vgas*sqrt(1./12.);
        
    }
    
    cout<<" general initialization done "<<endl;
}
