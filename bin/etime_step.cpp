//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

void devol::ts_hydr(double dt_pointer)
{
    dt = dt_pointer;
    if (shatterdestr==1){
    calc_CCC();// + shattering
    }
    for (int i=0; i< Nbin_a; i++)
    for (int j=0; j< Nbin_e; j++)                                                                   
    {
        if (photoar==1){
        Aij(i,j);   // + aromatization
        }
        if (photodestr==1){
        Rij(i,j);   // + photodestruction
        }
        if ((sputar==1)|(sputterdestr ==1)){
        Sij(i,j);   // + sputtering
        }
    }
    
    cvode_mem = NULL;
    y = abstol = NULL;

    y = N_VNew_Serial(Nbin_a*Nbin_e);
    abstol = N_VNew_Serial(Nbin_a*Nbin_e);
    int o=0;
    for (int i=0; i< Nbin_a; i++)
    for (int j=0; j< Nbin_e; j++)
    {
        Ith(y, o) = NNN[i][j];
        o++;
    }
    
    int flag, flagr, iout;
    
    realtype tloc(0.0), t, tout;
    double dtk = dt * _constant_yr2sec;
    
    cv_reltol = 1e-15;          //	No smaller than 1e-15
    
    o = 0;
    for (int i=0; i<Nbin_a; i++)
    {
        for (int j=0; j<Nbin_e; j++)
        {
            
            Ith(abstol, o) = 1.0e-15*pow(10,-i);
            o++;
        }
    }
    /* Call CVodeCreate to create the solver memory and specify the
     * Backward Differentiation Formula and the use of a Newton iteration */
    cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
    if (check_flag((void *)cvode_mem, "CVodeCreate", 0)) cin.get();

    /* Call CVodeInit to initialize the integrator memory and specify the
     * user's right hand side function in y'=f(t,y), the inital time T0, and
     * the initial dependent variable vector y. */
    flag = CVodeInit(cvode_mem, RF, tloc, y);
    if (check_flag(&flag, "CVodeInit", 1)) cin.get();
    
    /* Call CVodeSVtolerances to specify the scalar relative tolerance
     * and vector absolute tolerances */
    flag = CVodeSVtolerances(cvode_mem, cv_reltol, abstol);
    if (check_flag(&flag, "CVodeSVtolerances", 1)) cin.get();
    
    /* Call CVDense to specify the CVDENSE dense linear solver */
    flag = CVDense(cvode_mem, Nbin_e*Nbin_a);
    if (check_flag(&flag, "CVDense", 1)) cin.get();
    
    /* Set the Jacobian routine to Jac (user-supplied) */
    //flag = CVDlsSetDenseJacFn(cvode_mem, Jac);
    //if (check_flag(&flag, "CVDlsSetDenseJacFn", 1)) cin.get();
    
    flag = CVodeSetUserData(cvode_mem, (void*)this);
    if (check_flag(&flag, "CVodeSetUserData", 1)) cin.get();
    
    flag = CVodeSetMaxNumSteps(cvode_mem, 3000000);
    if (check_flag(&flag, "CVodeSetMaxNumSteps", 1)) cin.get();
    /* In loop, call CVode, print results, and test for error.
     Break out of loop when NOUT preset output times have been reached.  */
    iout = 0;  tout = dtk;
    int imax = int(T_max/dt);
    int nprint = int(T_max/DT);
    int iprint = imax/nprint;
    double Nt[Nbin_a], N0[Nbin_a];
    double tstep[Nbin_a], etstep[Nbin_a],tstephere, etstephere;
    tstephere = dtk;
    for (int kk=0; kk<Nbin_a; kk++){
        tstep[kk] = dtk;
        etimescale[kk]=0.;}
    while(1) {
        int o=0;
        for (int i=0; i< Nbin_a; i++)
            for (int j=0; j< Nbin_e; j++)
            {
                Ith(y, o) = NNN[i][j];
            }

        flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
        if (check_flag(&flag, "CVode", 1)) break;
        o=0;
        for (int kk=0; kk<Nbin_a; kk++){
            Nt[kk] = 0.;
            N0[kk] = 0.;
        }
        for (int i=0; i< Nbin_a; i++)
        {
            for (int j=0; j< Nbin_e; j++)
            {
                if (Ith(y,o)<0.){
                    Ith(y,o)=0.;
                }
                if (j==0)
                    Nt[i] += Ith(y,o);
                else if (j==4)
                    N0[i] += Ith(y,o);
                else{
                    Nt[i] += 0.;
                    N0[i] += 0.;
                }
                o++;
            }
            //cout<<"i = "<<i<<"N0 = "<<N0[i]<<" Nt = "<<Nt[i]<<endl;
        }
        for (int kk=0; kk<Nbin_a; kk++){
            double temp = (N0[kk]-Nt[kk])/N0[kk];
            //cout<<"N0 = "<<N0[kk]<<" Nt = "<<Nt[kk]<<" temp = "<<temp<<endl;
            
            if ((temp<0.5)&&(etimescale[kk]==0.)){
                cout<<"too long first step!"<<endl;
                etimescale[kk] = tout/_constant_yr2sec;
                tstep[kk] = T_max*_constant_yr2sec;
            }
            else if ((temp>=0.5)&&(temp<0.75)&&(etimescale[kk]==0.)){
                etimescale[kk] = tout/_constant_yr2sec;
                tstep[kk] = T_max*_constant_yr2sec;
            }
            else if ((temp>=0.75)&&(temp<0.999)&&(etimescale[kk]==0.)){
                tstep[kk] = tstephere;
            }
            else if ((temp>=0.999)&&(etimescale[kk]==0.)){
                tstep[kk] = tstephere*2;
            }
            

        }
        tstephere = min(tstep, Nbin_a);
        tout += tstephere;
        //cout<<"tstep = "<<tstephere/_constant_yr2sec<<endl;
        //cout<<"tout = "<<tout/_constant_yr2sec<<endl;
        //cin.get();
        if (tout/_constant_yr2sec >= T_max){
            for (int kk=0; kk<Nbin_a; kk++){
                if (etimescale[kk]==0.) {
                    etimescale[kk] = T_max;
                }
            }
            break;}
    }
    o=0;
    for (int i=0; i< Nbin_a; i++)
        for (int j=0; j< Nbin_e; j++)
        {
            if (Ith(y,o)<0.){
                Ith(y,o)=0.;
            }
            NNN[i][j] = Ith(y,o);
            o++;
        }

    total_output();
    cout<<nH<<" "<<T<<" "<<vgas<<" "<<vdust<<" "<<fieldfactor<<endl;
    cout<<"aromatization timescales: "<<endl;
    for (int i=0; i<Nbin_a; i++){
        cout<<"a = "<<a[i]<<" ts_arom = "<<etimescale[i]<<endl;
    }
    TNOW = tout;
    return;
}

