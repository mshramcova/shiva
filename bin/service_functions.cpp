//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"
#include "../header/service.h"

double devol::coulombian_factor(double charge1,double charge2,double a1,double a2,double m1,double m2,double vcol)
{
    double zel = 4.8032e-10;
    double m12 = m1*m2/(m1+m2);
    double factor = 1 - 2.*charge1*charge2*pow(zel,2)/m12/(a1+a2)/pow(vcol,2);
    if (factor<0.){
        factor = 0.;}
    return factor;
}


void atomic_data(int &z1_ion, double &chi_ion, double &m1_ion, double &E0n_ion, double &E0n_ion_h, double &L_ion, double &r2_ion, int &z2_pah, int &zion, double &m2_pah,char iontype)
{

    z2_pah = 6;
    zion=1;
    m2_pah = 12.009; //m1_c
    
    switch (iontype)
    {
        case 'h':
            chi_ion = 1;
            z1_ion = 1;
            m1_ion = 1.008;
            E0n_ion = 26.4; //PAHs
            //E0n_ion = 15.8; //HAC
            E0n_ion_h = 4.5;
            L_ion = 0.310;
            r2_ion = 2.28;
            
            return;
        case 'e':
            chi_ion = 0.1;
            z1_ion = 2;
            m1_ion = 4.003;
            E0n_ion = 10.;//PAHs
            //E0n_ion = 6.0; //HACs
            E0n_ion_h = 7.0;
            L_ion = 0.755;
            r2_ion = 0.88;
            
            return;
        case 'c':
            chi_ion = 1e-4;
            z1_ion = 6;
            m1_ion = 12.009;
            E0n_ion = 7.5; //PAHs
            //E0n_ion = 4.5; //HACs
            E0n_ion_h = 10.5;
            L_ion = 1.692;
            r2_ion = 0.90;
            
            return;
            
        default:
            cout<<"wrong iontype"<<endl;
            cin.get();
            break;
            
    }
            
}

double func_mion(char iontype)
{
    switch (iontype)
    {
        case 'h':
            
            return 1.008;
        case 'e':
            
            return 4.003;
        case 'c':
            
            return 12.009;
            
        default:
            cout<<"wrong iontype in mion"<<endl;
            cin.get();
            break;
    }
    return 0.0;
}


double planck_lambda(double la, double T)
{
    return 2.*_constant_hplanck*pow(_constant_clight,2)/pow(la,5)/(exp(_constant_hplanck*_constant_clight/la/_constant_boltz_sgs/T) - 1);
    
}
double min(double *a, int len_a)
{
	double minvalue = a[0];
	for (int i=0; i<len_a; i++)
		if (minvalue>a[i] && a[i]!=0.)
			minvalue=a[i];
    
	return minvalue;
}
double max(double *a, int len_a)
{
	double maxvalue = a[0];
	for (int i=0; i<len_a; i++)
		if (maxvalue<a[i])
			maxvalue=a[i];
    
	return maxvalue;
}

void logscale(double *p, double minvalue, double maxvalue, int nstep)
{
    
    double delta = pow(10., (1./double(nstep-1))*log10(maxvalue/minvalue));
    p[0] = minvalue;
    for(int i=1; i<nstep; i++)
        p[i] = p[i-1]*delta;
}

double interp_1d(double x, double *x1, double *y1, int n)
{
	double y;
	int i;
    if (x>=x1[0] && x<x1[n-1])
    {
        for (i = 0; i < n-1; i++)
        {
            if (x >= x1[i] && x < x1[i+1])
            {
                y = y1[i] + (y1[i+1] - y1[i])*(x - x1[i])/(x1[i+1] - x1[i]);
            }
        }
    }
    else if(x>=x1[n-1])
    {
        //cout<<" here"<<endl;
        y = y1[n-1];
    }
    else if(x<x1[0])
    {
        y = y1[0];
    }
    //if (y == 0.0) {
    else
    {
        cout<<" y not defined "<<x1[0]<<" "<<x1[n-1]<<" "<<x<<endl;
        cin.get();
    }
    //}
    return y;
}

double trapz(double arr_x[], double arr_y[], int len_arr_x,int n)
{
	double a = min(arr_x, len_arr_x);
	double b = max(arr_x, len_arr_x);
	double h,sum,x;
	int i;
	sum = 0;
	h=(b-a)/(double)(n);
	
    for(i = 0; i < n; i++)
	{
		x = a + (double)i*h;
		sum += 2*interp_1d(x, arr_x, arr_y, len_arr_x);
	}
	x = a + (double)n*h;
	sum += interp_1d(x, arr_x, arr_y, len_arr_x);
    return 0.5*h*sum;
    
}

int*AllocateInt(const int &n)
{
	try
	{
        int *a = new int [n];
        
		return a;
	}
	catch(bad_alloc)
	{
		cerr << "Out of memory" << endl;
		cin.get();
        
		return 0;
	}
}


double *Allocate(const int &n)
{
	try
	{
        double *a = new double [n];
        
		return a;
	}
	catch(bad_alloc)
	{
		cerr << "Out of memory" << endl;
		cin.get();
        
		return 0;
	}
}

char *Allocate_char(const int &n)
{
    try
    {
        char *a = new char [n];
        
        return a;
    }
    catch(bad_alloc)
    {
        cerr << "Out of memory" << endl;
        cin.get();
        
        return 0;
    }
}

int **AllocateInt(const int &n, const int &m)
{
	try
	{
		int **a = new int*[n];
        
        for(int i=0; i<n; i++)
			a[i] = new int [m];
		
        
		return a;
	}
	catch(bad_alloc)
	{
		cerr << "Out of memory" << endl;
		cin.get();
        
		return 0;
	}
}

double **Allocate(const int &n, const int &m)
{
	try
	{
		double **a = new double*[n];
        
        for(int i=0; i<n; i++)
			a[i] = new double [m];
		
        
		return a;
	}
	catch(bad_alloc)
	{
		cerr << "Out of memory" << endl;
		cin.get();
        
		return 0;
	}
}
//----------------------------------------------------------------------------------------------------------------------------
void DeAllocate2dArray(double **a, const int &n, const int &m)
{
	
    for(int i=0; i<n; i++)
        delete []a[i];
	
    
    delete []a;
    
}
//==============================================================================================================================
double *** Allocate(const int &n, const int &m, const int &l)
{
	double* A = (double*)calloc(n*m*l, sizeof(double));
    
    if(!A)
    {
        cerr << "Out of memory" << endl;
        cin.get();
        
        exit(0);
        
        return 0;
    }
    
    double*** a = (double***)calloc(n, sizeof(double**));
    
    for(int i = 0; i < n; i++)
        a[i] = (double**)calloc(m, sizeof(double*));
    
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            a[i][j] = A + i*l*m + j*l;
    
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            for(int k = 0; k < l; k++)
                a[i][j][k] = 0.0;
    
    return a;
}

int *** Allocate3dArrayInt(const int &n, const int &m, const int &l)
{
	int* A = (int*)calloc(n*m*l, sizeof(int));
    
    if(!A)
    {
        cerr << "Out of memory" << endl;
        cin.get();
        
        exit(0);
        
        return 0;
    }
    
    int*** a = (int***)calloc(n, sizeof(int**));
    
    for(int i = 0; i < n; i++)
        a[i] = (int**)calloc(m, sizeof(int*));
    
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            a[i][j] = A + i*l*m + j*l;
    
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            for(int k = 0; k < l; k++)
                a[i][j][k] = 0.0;
    
    return a;
    
}
//------------------------------------------------------------------------------

void DeAllocate3dArray(double ***a, const int &n, const int &m, const int &l)
{
	if(!a) return;
    
    
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            delete []a[i][j];
    
    
    for(int i = 0; i < n; i++)
        delete []a[i];
    
	delete []a;
}
double **** Allocate(const int &n, const int &m, const int &l, const int &s)
{
	double* A = (double*)calloc(n*m*l*s, sizeof(double));
    
	if(!A)
	{
		cerr << "Out of memory" << endl;
		cin.get();
        
		exit(0);
        
		return 0;
	}
    
	double**** a = (double****)calloc(n, sizeof(double***));
    
	for(int i = 0; i < n; i++)
		a[i] = (double***)calloc(m, sizeof(double**));
    
	for(int i = 0; i < n; i++)
		for(int j = 0; j < m; j++)
			a[i][j] = (double**)calloc(l, sizeof(double*));
    
	for(int i = 0; i < n; i++)
		for(int j = 0; j < m; j++)
			for(int k = 0; k < l; k++)
				a[i][j][k] = A + i*s*l*m + j*s*l + k*s;
    
	for(int i = 0; i < n; i++)
		for(int j = 0; j < m; j++)
			for(int k = 0; k < l; k++)
				for(int i_s = 0; i_s < s; i_s++)
					a[i][j][k][i_s] = 0.0;
    
	return a;
    
}


double ****** Allocate(const int &n1, const int &n2, const int &n3, const int &n4, const int &n5, const int &n6)
{
	double* A = (double*)calloc(n1*n2*n3*n4*n5*n6, sizeof(double));
    
	if(!A)
	{
		cerr << "Out of memory" << endl;
		cin.get();
        
		exit(0);
        
		return 0;
	}
    
	double****** a = (double******)calloc(n1, sizeof(double*****));
    
	for(int i = 0; i < n1; i++)
		a[i] = (double*****)calloc(n2, sizeof(double****));
    
	for(int i = 0; i < n1; i++)
    for(int j = 0; j < n2; j++)
        a[i][j] = (double****)calloc(n3, sizeof(double***));
    
	for(int i = 0; i < n1; i++)
    for(int j = 0; j < n2; j++)
    for(int k = 0; k < n3; k++)
        a[i][j][k] = (double***)calloc(n4, sizeof(double**));
    
	for(int i = 0; i < n1; i++)
    for(int j = 0; j < n2; j++)
    for(int k = 0; k < n3; k++)
    for(int l = 0; l < n4; l++)
        a[i][j][k][l] = (double**)calloc(n5, sizeof(double*));
    
	for(int i = 0; i < n1; i++)
    for(int j = 0; j < n2; j++)
    for(int k = 0; k < n3; k++)
    for(int l = 0; l < n4; l++)
    for(int m = 0; m < n5; m++)
            a[i][j][k][l][m] = A + i*n2*n3*n4*n5*n6 + j*n3*n4*n5*n6 + k*n4*n5*n6 + l*n5*n6 + m*n6;
    
	for(int i = 0; i < n1; i++)
    for(int j = 0; j < n2; j++)
    for(int k = 0; k < n3; k++)
    for(int l = 0; l < n4; l++)
    for(int m = 0; m < n5; m++)
    for(int o = 0; o < n6; o++)
        a[i][j][k][l][m][o] = 0.0;
    
	return a;
    
}



