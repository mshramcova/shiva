//
//  Developed by Maria Murga
//

#include "../header/dust_evolition_pro.h"


double devol::optics_qabs(double l, double aa, double eg, double Z)
{
    
    
    //   cout<<"opt qabs "<<endl;
    l *= 1e4; /* ---  la in mkm   */
    
    if (strcmp(graintype,"hac")==0){

    int i1(nsize-2),i1l(0),i1r(nsize-2);
    int i2(ngap- 2),i2l(0),i2r(ngap- 2);
    int i3(nwave-2),i3l(0),i3r(nwave-2);
    int iz;
    double d3;
    double dx1,dx2,dy1,dy2,dz1,dz2;
    
    
    if(aa<=size[0])
    {
        i1=0;
        
        i1l=0;
        i1r=0;
        dx1 = 0.5;
        dx2 = 0.5;
        
    }
    else if(aa>=size[nsize-1])
    {
        i1=nsize-1;
        i1l=nsize-1;
        i1r=nsize-1;
        dx1 = 0.5;
        dx2 = 0.5;
    }
    else
    {
        
        while(!(size[i1]<=aa && aa<=size[i1+1]))
        {
            
            i1=i1l+floor((i1r-i1l)/2);
            
            if(size[i1]<=aa)
                i1l=i1;
            else
                i1r=i1;
        };
        
        i1l=i1;
        i1r=i1+1;
        dx1 = (aa-size[i1])/(size[i1+1]-size[i1]);
        dx2 = (size[i1+1]-aa)/(size[i1+1]-size[i1]);
    }
    
    
    if(eg<=egfull[0])
    {
        i2=0;
        
        i2l=0;
        i2r=0;
        dy1=0.5;
        dy2=0.5;
        
    }
    else if(eg>=egfull[ngap-1])
    {
        i2=ngap-1;
        i2l=ngap-1;
        i2r=ngap-1;
        dy1=0.5;
        dy2=0.5;
    }
    else
    {
        
        while(!(egfull[i2]<=eg && eg<=egfull[i2+1]))
        {
            
            i2=i2l+floor((i2r-i2l)/2);
            
            if(egfull[i2]<eg)
                i2l=i2;
            else
                i2r=i2;
            
        };
        
        i2l=i2;
        i2r=i2+1;
        dy1=(eg-egfull[i2])/(egfull[i2+1]-egfull[i2]);
        dy2=(egfull[i2+1]-eg)/(egfull[i2+1]-egfull[i2]);
    }
  
    if(l<=wave_opt[0])
    {
        i3=0;
        i3l = 0;
        i3r = 0;
        dz1=0.5;
        dz2=0.5;
    }
    else if(l>=wave_opt[nwave-1])
    {
        i3 = nwave-1;
        i3l = nwave-1;
        i3r = nwave-1;
        dz1=0.5;
        dz2=0.5;
    }
    else
    {
        while(!(wave_opt[i3]<=l && l<=wave_opt[i3+1]))
        {
            
            i3=i3l+floor((i3r-i3l)/2);
            
            if(wave_opt[i3]<=l)
                i3l=i3;
            else
                i3r=i3;
        };

        //d3 = log10(wave_opt[1])-log10(wave_opt[0]);
        //i3 = floor((log10(l)-log10(wave_opt[0]))/d3);
        
        i3l = i3;
        i3r = i3+1;
        dz1=(l-wave_opt[i3])/(wave_opt[i3+1]-wave_opt[i3]);
        dz2=(wave_opt[i3+1]-l)/(wave_opt[i3+1]-wave_opt[i3]);
        //cout<<"i3 = "<<i3<<endl;
        //cout<<"wave_opt "<<wave_opt[i3]<<" "<<wave_opt[i3+1]<<" l = "<<l<<endl;
    }
 
    //cout<<i1l<<" "<<i2l<<" "<<i3l<<endl;
    //cout<<i1r<<" "<<i2r<<" "<<i3r<<endl;
    //cout<<dx1<<" "<<dy1<<" "<<dz1<<endl;
    //cout<<dx2<<" "<<dy2<<" "<<dz2<<endl;
    //cout<<"qabs= "<<qabs_opt[i1l][i2l][i3l]<<" "<<qabs_opt[i1l][i2l][i3r]<<" "<<qabs_opt[i1l][i2r][i3r]<<endl;
    
    //cin.get();
    
    if (Z<=0){
        iz = 0;}
    else{
        iz=1;}
    return qabs_opt[iz][i1l][i2l][i3l] * dx2*dy2*dz2+
           qabs_opt[iz][i1r][i2l][i3l] * dx1*dy2*dz2+
           qabs_opt[iz][i1l][i2r][i3l] * dx2*dy1*dz2+
           qabs_opt[iz][i1r][i2r][i3l] * dx1*dy1*dz2+
           qabs_opt[iz][i1l][i2l][i3r] * dx2*dy2*dz1+
           qabs_opt[iz][i1r][i2l][i3r] * dx1*dy2*dz1+
           qabs_opt[iz][i1l][i2r][i3r] * dx2*dy1*dz1+
           qabs_opt[iz][i1r][i2r][i3r] * dx1*dy1*dz1;
    }
    
    
    else if (strcmp(graintype,"gra")==0){
        
        int i1(nsize_pah-2),i1l(0),i1r(nsize_pah-2);
        int i3(nwave-2),i3l(0),i3r(nwave-2);
        int iz;
        double d3;
        double dx1,dx2,dz1,dz2;
        
        
        if(aa<=size_pah[0])
        {
            i1=0;
            
            i1l=0;
            i1r=0;
            dx1 = 0.5;
            dx2 = 0.5;
            
        }
        else if(aa>=size_pah[nsize_pah-1])
        {
            i1=nsize_pah-1;
            i1l=nsize_pah-1;
            i1r=nsize_pah-1;
            dx1 = 0.5;
            dx2 = 0.5;
        }
        else
        {
            
            while(!(size_pah[i1]<=aa && aa<=size_pah[i1+1]))
            {
                
                i1=i1l+floor((i1r-i1l)/2);
                
                if(size_pah[i1]<=aa)
                    i1l=i1;
                else
                    i1r=i1;
            };
            
            i1l=i1;
            i1r=i1+1;
            dx1 = (aa-size_pah[i1])/(size_pah[i1+1]-size_pah[i1]);
            dx2 = (size_pah[i1+1]-aa)/(size_pah[i1+1]-size_pah[i1]);
        }
        
        
        if(l<=wave_opt[0])
        {
            i3=0;
            i3l = 0;
            i3r = 0;
            dz1=0.5;
            dz2=0.5;
        }
        else if(l>=wave_opt[nwave-1])
        {
            i3 = nwave-1;
            i3l = nwave-1;
            i3r = nwave-1;
            dz1=0.5;
            dz2=0.5;
        }
        else
        {
            while(!(wave_opt[i3]<=l && l<=wave_opt[i3+1]))
            {
                
                i3=i3l+floor((i3r-i3l)/2);
                
                if(wave_opt[i3]<=l)
                    i3l=i3;
                else
                    i3r=i3;
            };
            
            //d3 = log10(wave_opt[1])-log10(wave_opt[0]);
            //i3 = floor((log10(l)-log10(wave_opt[0]))/d3);
            
            i3l = i3;
            i3r = i3+1;
            dz1=(l-wave_opt[i3])/(wave_opt[i3+1]-wave_opt[i3]);
            dz2=(wave_opt[i3+1]-l)/(wave_opt[i3+1]-wave_opt[i3]);
            //cout<<"i3 = "<<i3<<endl;
            //cout<<"wave_opt "<<wave_opt[i3]<<" "<<wave_opt[i3+1]<<" l = "<<l<<endl;
        }
        
        //cout<<i1l<<" "<<i3l<<endl;
        //cout<<i1r<<" "<<i3r<<endl;
        //cout<<dx1<<" "<<dz1<<endl;
        //cout<<dx2<<" "<<dz2<<endl;
        //cout<<"qabs= "<<qabs_opt_pah[0][i1l][i3l]<<" "<<qabs_opt_pah[0][i1l][i3r]<<" "<<endl;
        
        //cin.get();
        
        if (Z<=0){
            iz = 0;}
        else{
            iz=1;}
        return qabs_opt_pah[iz][i1l][i3l] * dx2*dz2+
        qabs_opt_pah[iz][i1r][i3l] * dx1*dz2+
        qabs_opt_pah[iz][i1l][i3r] * dx2*dz1+
        qabs_opt_pah[iz][i1r][i3r] * dx1*dz1;

        
    }
    else return 0;
    
 }

