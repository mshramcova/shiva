//
//  Developed by Maria Murga 
//
// ---------------- dust_evolution_pro.cpp ---------------------------
#include "../header/dust_evolition_pro.h"



int main(void)
{

    devol odev;

    odev.read_initial_data();
    
    odev.general_intialization();
    
    odev.read_initial_data2();
    
    odev.total_output();
    
    odev.TNOW = 0.0;
    
    if (odev.sd_output == 1)    odev.integrator(odev.dt);
    
    if (odev.ts1 == 1){
        odev.general_intialization();
        odev.TNOW = 0.0;
        odev.ts_carb(odev.dt_step);
    }
    if (odev.ts2 == 1){
        odev.general_intialization();
        odev.TNOW = 0.0;
        odev.ts_hydr(odev.dt_step);
    }
    odev.ts_output();
}
