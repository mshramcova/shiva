//
//  Developed by Maria Murga 
//
#include "../header/dust_evolition_pro.h"


void devol::calc_CCC(void)
{

    double a1a2, v, mnew1, mnew2, mold;
    double P1, c0, Pv, s_shat, v_shat;
    shattering_data(P1, c0, Pv, s_shat, v_shat);
    
    for (int i1=0; i1< Nbin_a; i1++)
    for (int j1=0; j1< Nbin_e; j1++)
    for (int i2=0; i2< Nbin_a; i2++)
    for (int j2=0; j2< Nbin_e; j2++)
    {

        v = pow(vel_of_bin[i1][j1] - vel_of_bin[i2][j2],2.0)+
        pow(dsp_of_bin[i1][j1] + dsp_of_bin[i2][j2],2.0);
        
        v = sqrt(v);
        //cout<<"v = "<<v<<endl;
        for (int I=0; I< Nbin_a; I++)
        for (int J=0; J< Nbin_e; J++)
        {
            if (v >= v_shat)
            {

                mshat(mnew1, mnew2, mold, m[i1], m[i2], a[i1], a[i2], v, I, J, j1, j2);
                CCC[i1][j1][i2][j2][I][J] = (mnew1+mnew2+mold)/m[I];
                /*if ((I==15)&&(CCC[i1][j1][i2][j2][I][J]>1))
                
                    {
                    //cout<<"m[I] = "<<m[I]<<endl;
                        cout<<"I= "<<I<<" J= "<<J<<" i1 = "<<i1<<" j1 = "<<j1<<" i2 = "<<i2<<" j2= "<<j2<<endl;
                        cout<<"m[i2] = "<<m[i2]<<" m[i1] = "<<m[i1]<<"mnew1 = "<<mnew1<<" mnew2= "<<mnew2<<" mold = "<<mold<<" m[I]= "<<m[I]<<endl;
                        cout<<"CCC = "<<CCC[i1][j1][i2][j2][I][J]<<endl;
                        //cin.get();
                 
                    }*/
            }
            else{
                CCC[i1][j1][i2][j2][I][J] =0.0;
            }
        }
        a1a2 = a[i1] + a[i2];
    
        if (v >= v_shat)
        {
            
            LLL[i1][j1][i2][j2] = M_PI * a1a2 * a1a2 * v* coulombian_factor(charge[i1],charge[i2],a[i1],a[i2],m[i1],m[i2],v);
            
        }
        else{
            LLL[i1][j1][i2][j2] = 0.0;
        }

    }
}


void devol::mshat(double &mshat1, double &mshat2, double &mold,
                  double m1, double m2, double a1, double a2,
                  double v,
                  int i_a, int i_eg, int i_subeg1, int i_subeg2)
{
	int n_steps = nbin_shat;
    int idx_len = 0;
    double adop, P1, c0, s_shat, v_shat, Pv;
    int bdop;
    if (m1<m2){
        adop = m2;
        m2=m1;
        m1=adop;
        adop = a2;
        a2=a1;
        a1=adop;
        bdop = i_subeg2;
        i_subeg2=i_subeg1;
        i_subeg1=bdop;

    }
    //  ----------  NEW FIXES ------------------
    
    i_a = i_a + 1;
    v = v + 0.1;
    
    shattering_data(P1, c0, Pv, s_shat, v_shat);
    phi1 = P1/ro/pow(c0,2.0);
    double aleft = ab[i_a-1];
    double aright= ab[i_a];
    
    double mleft = mb[i_a-1];
    double mright= mb[i_a];
 
  
//    cout<<i_a<<" "<<aleft<<" "<<aright<<endl;
//    cout<<i_a<<" "<<mleft<<" "<<mright<<endl;
	////m2 - projectile particle
	////m1 - target particle
	//// Calculate the shattered mass from the target grain (mshat1)
    
	double R(1.0);                         // For collision between the same species
	double Mach_1, Mach_r;                      // The Mach number corresponding to critical pressure P1
	double sigmar, sigma1;                      // constants sigma
	double M;                                   // M the mass shocked to the critical pressure P1 for cratering in the target
    
	mold = 0.0;                                	// Mej is the total mass of fragments ejected from m1.
    
	double vcat, vcat2, af_max, af_min, af_max2, af_min2;
	
	double C, Mej, z, mshat_dop, aold;
	
	
    Mach_r = v/c0;
    Mach_1 = 2.0*phi1/(1.0+pow((1.0+4.0*s_shat*phi1), 0.5));
    
    sigmar = 0.3*pow((s_shat+1.0/Mach_r*(1.0+R)-0.11), 1.3)/(s_shat+1.0/Mach_r*(1.0+R)-1.0);  	//// constants sigma
	sigma1 = 0.3*pow((s_shat+1.0/Mach_1-0.11), 1.3)/(s_shat+1.0/Mach_1-1.0);
    
    M = m2 * (1.0+2.0*R)/2.0/pow((1.0+R),(16.0/9.0))/pow(sigmar,(1.0/9.0))
    *pow((pow(Mach_r, 2.0)/sigma1/pow(Mach_1,2.0)),(8.0/9.0));
    
    z = 3.4;
	mshat_dop = 0.0;
	aold = 0.0;
    
	if(i_subeg1 == i_eg)
	{
		if (M >= m1/2.)
		{
			Mej = m1;
			vcat = c0*pow((m1/(1.0+2.0*R)/m2), (9.0/16.0))*sqrt(sigma1)*pow(sigmar,0.0625)*(1.0+R)*Mach_1;
			
			af_max = 0.22*a1*(vcat/v);
			af_min = 0.03*af_max;
        }
        else
		{
			Mej = 0.4 * M;
			
			//// The maximum of fragment size is af_max:
			af_max  = pow((Mej/ro*3./16./M_PI/pow(z,3.0)/(z-2)*(z+1)),(1./3.));
			
            //// The grains are shattered to a number of smaller grains.
			//// The minimum size is:
			af_min = af_max*pow((P1/Pv),1.47);
        }
        logscale(ai2, af_min, af_max, nbin_shat);
        
        //// Constant for size distribution
        
        for(int i = 0; i < nbin_shat; i++)
        {    ai_ro[i] = 4.0/3.0*M_PI*pow(ai2[i],3.0)*ro*pow(ai2[i],-3.3);
            
        }
        //    cout<<nbin_shat<<" "<<n_steps<<endl;
        C = Mej/trapz(ai2, ai_ro, nbin_shat, n_steps);
        
        for(int i = 0; i < nbin_shat; i++)
            nshat[i] = C*pow(ai2[i],(-3.3));
        
        set_idx(ai2, idx, nbin_shat, idx_len, aleft, aright);
        
        if (idx_len == 0)
            mshat1 = 0.0;
        else
            if (idx_len == 1)
                mshat1 = 4.0/3.0*M_PI*pow(ai2[idx[0]],3.0)*ro;
            else
            {
                init_arr_by_idx(ai_idx, ai2, idx, idx_len);
                init_arr_by_idx(nshat_idx, nshat, idx, idx_len);
                
                for(int i = 0; i < idx_len; i++){
                    ai_ro_idx[i] = 4.0/3.0*M_PI*pow(ai_idx[i],3.0)*ro*nshat_idx[i];
                    //cout<<" ai_idx = "<<ai_idx[i]<<" afmin = "<<af_min<<" afmax = "<<af_max<<endl;
                    //cout<<"aleft = "<<aleft<<" aright= "<<aright<<endl;}
                    }
                mshat1 = trapz(ai_idx, ai_ro_idx, idx_len, n_steps);
            }
        
        for(int i = 0; i < nbin_shat; i++)
            ai_ro[i] = 4.0/3.0*M_PI*pow(ai2[i],3.0)*ro*nshat[i];
        mshat_dop = trapz(ai2, ai_ro, nbin_shat, n_steps);
        
        
        if (M < m1/2.)
        {
            ////The mass that left after ejection mold
            if ((m1 - Mej>= mleft) && (m1 - Mej < mright))
                mold = m1 - Mej;
        }
        else{
            mold=0.0;
        }
         
         //cout<<"check shattering mshatdop = "<<mshat_dop<<" mold = "<<mold<<" m1 = "<<m1<<" mshat1 = "<<mshat1<<" mleft = "<<mleft<< " mright = "<<mright<<endl;
        //cin.get();
        /*if(((af_min>aleft)&&(af_min<aright))||((af_max>aleft)&&(af_max<aright))){
            cout<<"m1 = "<<m1<<" m = "<<m[i_a-1]<<" mshat1 = "<<mshat1<<endl;
            cout<<" i_eg = "<<i_eg<<" i_subeg1 = "<<i_subeg1<<endl;
            cout<<"idx_len = "<<idx_len<<endl;
            cout<<"a1 = "<<a1<<" af_min = "<<af_min<<" af_max = "<<af_max<<endl;
            cout<<"aleft = "<<aleft<<"aright = "<<aright<<endl;
            cin.get();
        }*/

        
	}
	else
	{
		mshat1 = 0.0;
		mold = 0.0;
	}
    
    
    
	////Calculate the shattered mass of projectile grain (mshat2) which is destroyed fully as assumed, i.e Mej = m2:
	if (i_subeg2 == i_eg)
	{
		
        vcat2 = c0*pow((m1/(1.0+2.0*R)/m2), (9.0/16.0))*sqrt(sigma1)*pow(sigmar,0.0625)*(1.0+R)*Mach_1;
        double vhere = vcat2;
		af_max2 = 0.22*a2*(vcat2/vhere);
		af_min2 = 0.03*af_max2;
        //cout<<"af_min2 = "<<af_min2<<" af_max2= "<<af_max2<<" a2 = "<<a2<<endl;
        //cin.get();
        logscale(ai2, af_min2, af_max2, nbin_shat);
		////#Constant for size distribution
        
        for(int i = 0; i < nbin_shat; i++)
            ai_ro[i] = 4.0/3.0*M_PI*pow(ai2[i],3.0)*ro*pow(ai2[i],(-3.3));
		
        C = m2/trapz(ai2, ai_ro, nbin_shat, n_steps);
        
		//nshat2 = C2*ai2**(-3.3)
		for(int i = 0; i < nbin_shat; i++) nshat[i] = C*pow(ai2[i],(-3.3));
		
		//idx2 = (ai2 >= aleft) & (ai2 < aright)
		set_idx(ai2, idx, nbin_shat, idx_len, aleft, aright);
        
		if (idx_len == 0) mshat2 = 0.0;
		else
            if (idx_len == 1)
                mshat2 = 4.0/3.0*M_PI*pow(ai2[idx[0]],3.0)*ro;
            else
            {
                init_arr_by_idx(ai_idx, ai2, idx, idx_len);
                init_arr_by_idx(nshat_idx, nshat, idx, idx_len);
                
                for(int i = 0; i < idx_len; i++)
                    ai_ro_idx[i] = 4.0/3.0*M_PI*pow(ai_idx[i],3.0)*ro*nshat_idx[i];
                
                mshat2 = trapz(ai_idx, ai_ro_idx, idx_len, n_steps);
            }
        for(int i = 0; i < nbin_shat; i++) ai_ro[i] = 4.0/3.0*M_PI*pow(ai2[i],3.0)*ro*nshat[i];
        mshat_dop = trapz(ai2, ai_ro, nbin_shat, n_steps);

	}
	else mshat2 = 0.0;
	

}

void devol::set_idx(double *arr_in, int* arr_idx, int size, int &len, double a_left, double a_right)
{
	int i_idx = 0;
	
    for(int i = 0; i < size; i++)
    {
        if ((arr_in[i] >= a_left) && (arr_in[i] < a_right))
            arr_idx[i_idx++] = i;
    }
    len = i_idx;
}

void devol::init_arr_by_idx(double *arr, double *arr_in, int *arr_idx, int len)
{
	for(int i = 0; i < len; i++)
        arr[i] = arr_in[arr_idx[i]];
}

void devol::density(double &ro)
{
    if (strcmp(graintype,"hac")==0){
        ro = 1.5;
    }
    else if(strcmp(graintype,"gra")==0){
        ro =  2.2;
    }
    else if (strcmp(graintype,"sil")==0){
        ro = 3.3;
    }
    return;
}

void devol::shattering_data(double &P1, double &c0, double &Pv, double &s_shat, double &v_shat){
    if (strcmp(graintype,"hac")==0){
        P1 = 2e10; //DYN/CM2
        c0 = 1.0e5; //cm/s
        Pv = 2.8e12;      //DYN/CM2
        s_shat = 1.9;
        v_shat = 0.8e5;   //cm/s
        return;
    }
    else if(strcmp(graintype,"gra")==0){
        P1 = 4e10; //DYN/CM2
        c0 = 1.8e5; //cm/s
        Pv = 5.8e12;      //DYN/CM2
        s_shat = 1.9;
        v_shat = 1.2e5;   //cm/s
        return;
    }
    else if (strcmp(graintype,"sil")==0){
        
        P1 = 3e11; //DYN/CM2
        c0 = 5.0e5; //cm/s
        Pv = 5.4e12;      //DYN/CM2
        s_shat = 1.2;
        v_shat = 2.7e5;   //cm/s
        return;
    }
}


