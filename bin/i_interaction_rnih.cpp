//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

void devol::i_interaction_ri(double &rni, double &rni_h, double &rei, double &rei_h,
                             char iontype, double apah, double Nc0,
                             double eg, double nH, double vpah,  double z_charge)
{
    
        
    double Np, Ncp, Nhp;
    
	double chi_ion, m1_ion, E0n_ion, E0n_ion_h, L_ion, r2_ion, m2_pah, M1;
    double m2_h = 1.;
    double z2_h = 1.;
	int z1_ion, z2_pah, zion;
	
    double snu, ascr, ascr_h;
	double vion, Eion, epsilon, mm, m;
    double gamma, sigma, avE, Fc;
    int Ncol;
    double xh = eg/4.3;

    atomic_data(z1_ion, chi_ion, m1_ion, E0n_ion, E0n_ion_h, L_ion, r2_ion, z2_pah, zion, m2_pah,iontype);
    m2_pah = 10.; // I guess that hydrogen is also can be ejected
    z2_pah = 5;
    
    if (strcmp(graintype,"gra")==0){
        Np=1;
        m2_pah = 12.; // I guess that hydrogen is also can be ejected
        z2_pah = 6;
        
    }
    else if(strcmp(graintype,"hac")==0){
        Np = ceil(pow(Nc0,1/3.)-1); //number of planes
        m2_pah = 12-11*xh; // I guess that hydrogen is also can be ejected
        z2_pah = 6 - 5.*xh;
        
    }
    Ncp = ceil(pow(Nc0,2/3.)); // number of carbon atoms per plane
    Nhp = ceil(Ncp*xh/(1. - xh));
    ascr = 0.885*_constant_a_bohr/(pow(z1_ion,0.23) + pow(z2_pah,0.23));  //screening length
    
    Fc = 1. - zion*z_charge*pow(_constant_ze,2)/_constant_e0/apah/m1_ion/_constant_aem/(vpah*vpah); //Fc - coulombian factor
    //E - kinetic energy of ion in eV, vion is equal to vpah, because it is relative velocity where PAH is stopped
    vion = vpah; //cm/s
    Eion = m1_ion*_constant_aem*vion*vion*0.5*6.242e11;  //eV
    
    
    if(Eion < E0n_ion)
    {rni = 0.;
        rni_h = 0.;}
    else
    {
        //epsilon - Lindhard's reduced energy
        epsilon = m2_pah/(m1_ion+m2_pah)*ascr/z1_ion/z2_pah/_constant_ze2*Eion;
        //reduced nuclear stopping cross section, it is adopted Ziegler-Biersack-Littmark nuclear stopping cross section
        if (epsilon <= 30)
            snu = 0.5*log(1.+1.1383*epsilon)/(epsilon+0.01321*pow(epsilon,0.21226)+0.19593*pow(epsilon,0.5));
        else
            snu = log(epsilon)/2./epsilon;
        
        //m - magic quantity, it's determined by Ziegler'85
        
        mm=0.0;
        for (int j=0; j<nai; j++)
            mm += ai[j]*pow(0.1*log10(epsilon*1e9),j);
        m = 1.0 - exp(-exp(mm));
        
        //gamma - is a magic parameter that show the power of collision,  thas is in maximum at head-pn collision
        gamma = 4.*m1_ion*m2_pah/pow(m1_ion+m2_pah,2);
        
        //sigma(E) - cross section averaged over those collisions that transfer an energy larger than the threshold T0 per C-atom
        sigma = 4.*M_PI*ascr*z1_ion*z2_pah*_constant_ze2*m1_ion/(m1_ion+m2_pah)*snu*(1.-m)/m/gamma/Eion*(pow(E0n_ion/Eion,-m)-1.);
        
        //average transferred energy
        avE = m/(1.-m)*gamma*(pow(Eion,1.-m)-pow(E0n_ion,1.-m))/(pow(E0n_ion,-m) - pow(gamma*Eion,-m));
        
        //Maximum of collisions that lead to ejection of carbon atom
        Ncol = int(min(Eion/avE, Np));
        if (Ncol<1)
            Ncol=1;
        //rate of inertial collision PAH-ion through the shock [s^-1]
        
        rni = (1-xh)*n_ion(iontype) * vpah * sigma * Ncp *Ncol * Fc *1e-16;
        rni_h = xh * n_ion(iontype) * vpah * sigma * Ncp *Ncol * Fc *1e-16;
        if (strcmp(graintype,"gra")==0){
            rni *= 0.5;
            rni_h *= 0.5;
        }
        
        if (rni < 0)
            rni = 0.;
        if (rni_h < 0)
            
            rni_h = 0.;
        switch (iontype)
        {
            case 'c':
                rni_h = 0.;
        }
    }
    
    /*----------------------------------------------------------------------------------------*/
    
    /*---------------------- Ion collisions: electronic interaction -----------------------------*/
    double p_c, p_h, p_h2, sfull, Ee;
    
        
	//d - thickness of PAH molecule
	double d = 4.61e-8; // cm
    
    for (int i = 0; i<nteta; i++)
    if (strcmp(graintype,"gra")==0){
        sigma_g[i] = M_PI*pow(apah,2)*cteta[i]+2*apah*d*steta[i];
    }
    else if (strcmp(graintype,"HAC")==0){
        sigma_g[i] = pow(2*apah,2)*(cteta[i]+steta[i]); }//geometrical cross section

	double apah_au = apah/0.529e-8;
	double d_au = d/0.529e-8;
	double vion_au = 0.2*sqrt(Eion/1e3/m1_ion);
	const int ns = 20;
    double Nl;
    double Efull; // eV
    
    double Y_c2h2;
    
	for (int i =0; i<nteta; i++)
	{
		if (fabs(tteta[i]) < 0.5*M_PI - atan(0.5*d/apah))
		{
			for (int j=0; j<ns; j++)
				it_seems_that_strange_s[j] = -0.5*d_au/cteta[i] + j*d_au/cteta[i]/double(ns-1);
			
            sfull = d_au/cteta[i];
 		}
		else
        {
            
            if (fabs(tteta[i]) == 0.5*M_PI - atan(0.5*d/apah))
            {
                for (int j=0; j<ns; j++)
                    it_seems_that_strange_s[j] = -sqrt(pow(d_au,2)+2.*pow(apah_au,2))/2. + j*sqrt(pow(d_au,2)+2.*pow(apah_au,2))/double(ns-1);
			
            sfull = sqrt(pow(d_au,2)+2.*pow(apah_au,2));
            }
            else
            {
            
                for (int j=0; j<ns; j++)
                    it_seems_that_strange_s[j] = -apah_au/steta[i] + j * 2*apah_au/steta[i]/double(ns-1);
			
                sfull = 2.*apah_au/steta[i];
            }
        }
		//L - damping width, I take some number's from Puska'83
		for (int j=0; j<ns; j++)
        {
            rs[j] = pow(4/3.*M_PI*0.15*exp(-pow(it_seems_that_strange_s[j]*cteta[i],2)/2.7),-1/3.);
            gamma2[j] = vion_au*L_ion*exp(-(rs[j]-1.5)/r2_ion);
        }
		
		Ee = 27.2116*trapz(it_seems_that_strange_s, gamma2,ns, 100);
        

        
        //p is total probability for dissociation upon collision via electronic excitation
        Nl = ceil(sfull*cteta[i]/d_au);
        Efull = Nl*Ee; // eV
        
        diss_rate(p_c, p_h, p_h2, xh, Nc0, Efull, apah, z_charge);
            
        for_int_rei_c[i] = sigma_g[i]*p_c*steta[i];
        for_int_rei_h[i] = sigma_g[i]*(p_h+2*p_h2)*steta[i];
    }
    rei = 2.*vion*n_ion(iontype)*Fc*trapz(teta, for_int_rei_c,nteta, 50);
    rei_h = 2.*vion*n_ion(iontype)*Fc*trapz(teta, for_int_rei_h,nteta, 50);
    if (rei < 0 || isnan(rei) != 0)
        rei = 0.;
    if (rei_h < 0 || isnan(rei_h) != 0)
        rei_h = 0.;
    
	/*----------------------------------------------------------------------------------*/
}
void devol::e_interaction_re(double & re_c_t, double & re_h_t, double Nc0, double apah, double nH, double T, double eg, double z_charge)
{
    
	//d - thickness of PAH molecule
	double d = 4.61e-8; //cm
	// this corresponds to electronic dissociation energy
    double vmin = sqrt(2*_constant_E0/(_constant_me*_constant_aem*6.242e11));
	double vmax = 100000e5; //it is taken "ot baldy :)"
	double apah_au = apah/0.529e-8;
	double d_au = d/0.529e-8;

	double re_c, re_h, Ee0, s, Fe0, Fe1, Eelec, Ee1, Efull, p_c, p_h,p_h2, Nl, Fc;
    double xh = eg/4.3;
    
    
    
	for (int i = 0; i<nv; i++)
	{
		//ve[i] = vmin + double(i)*(vmax -vmin)/double(nv-1);
        logscale(ve,vmin, vmax, nv);
        //density probability function for velocities
		fv[i] = 4.0*M_PI*pow(ve[i],2.0)*
                       pow(_constant_me*_constant_aem/2./M_PI/_constant_boltz/T/1.6e-12,3./2.)*
                       exp(-_constant_me*_constant_aem*pow(ve[i],2.0)/2./_constant_boltz/T/1.6e-12);
	}
	
	//cross section
    for (int i = 0; i<nteta; i++)
        if (strcmp(graintype,"gra")==0){
            sigma_g[i] = M_PI*pow(apah,2)*cteta[i]+2*apah*d*steta[i];
        }
        else if (strcmp(graintype,"HAC")==0){
            sigma_g[i] = pow(2*apah,2)*(cteta[i]+steta[i]);}
	for (int i_v=0; i_v<nv; i_v++)
	{
		//initial energy of electron
        Ee0 = _constant_me*_constant_aem*pow(ve[i_v],2)/2.*6.242e11;
        if (Ee0>10.)
		{
			for (int i=0; i<nteta; i++)
			{
                
                if (fabs(tteta[i]) < M_PI/2.-atan(d/2./apah))
					s = d_au/2./cteta[i];
				else
                    if (fabs(tteta[i]) == M_PI/2.-atan(d/2./apah))
                        s = sqrt(pow(d_au,2)+2.*pow(apah_au,2));
                    else
                        if (fabs(tteta[i]) > M_PI/2.-atan(d/2./apah))
                            s = 2.*apah_au/steta[i];
				

                s *= 0.529; //A
				Fe0 = interp_1d(Ee0, E, F, lenE);
				Fe1 = Fe0 - s;
				//Transferred energy from an electron to molecule
				if (Fe1 <= 0.)
					Eelec = Ee0;
				else
				{
					Ee1 = interp_1d(Fe1, F, E, lenE);
					Eelec = Ee0 - Ee1;
				}
                
                Nl = ceil(s*cteta[i]/d_au);
                if (strcmp(graintype,"gra")==0){
                    Efull = Eelec;
                }
                else if(strcmp(graintype,"hac")==0){
                    Efull = Nl*Eelec; // eV //number of planes
                }
               
                diss_rate(p_c, p_h, p_h2, xh, Nc0, Efull, apah, z_charge);
                for_int_re_c[i] = sigma_g[i]*p_c*steta[i];
                for_int_re_h[i] = sigma_g[i]*(p_h+2*p_h2)*steta[i];
                
            }
            //Fc - coulombian factor
			Fc = 1. - 2.*(-1)*z_charge*pow(_constant_ze,2)/_constant_e0/apah/_constant_me/_constant_aem/pow(ve[i_v],2);
            
			re_c = ve[i_v]*nH*Fc*trapz(teta, for_int_re_c, nteta, 100);
            re_h = ve[i_v]*nH*Fc*trapz(teta, for_int_re_h, nteta, 100);
			re_c_fv[i_v] = re_c*fv[i_v];
            re_h_fv[i_v] = re_h*fv[i_v];
		}
		else
        {
			re_c_fv[i_v] = 0;
            re_h_fv[i_v] = 0;
        }
	}
    
	re_c_t = 2.*trapz(ve, re_c_fv, nv, nv);
	
    if (re_c_t < 0 || isnan(re_c_t) != 0)
        re_c_t = 0.;
    
    re_h_t = 2.*trapz(ve, re_h_fv, nv, nv);
    
    if (re_h_t < 0 || isnan(re_h_t) != 0)
        re_h_t = 0.;
 }

void devol::diss_rate(double &p_c, double &p_h, double &p_h2, double xh, double Nc0, double Efull, double apah, double z_charge)
{
    double Natoms,k_h_loss,k_h2_loss,k_c2h2_loss,kdiss, ktot, nh;
    nh = Nc0*xh/(1. - xh);
    Natoms = Nc0 + nh;
    //Rates of disintegration of H, H2 and C2H2 according to the RRK model
    if (Efull > _constant_ebind)  // emin = 2.9
    {
        if (xh<0.1){
            
            k_h_loss    = 1e16*pow(1.0-2.8/Efull,3*Natoms-7);
            k_h2_loss   = 1e16*pow(1.0-2.9/Efull,3*Natoms-7);
            k_c2h2_loss = 1e15*pow(1.0-2.9/Efull,3*Natoms-7);
            kdiss = k_h_loss+k_h2_loss+k_c2h2_loss;
            ktot = ( kir(apah, Natoms, Efull, z_charge) + kdiss)/(1.-Yion(Efull, z_charge, apah));
            
            p_c = k_c2h2_loss/ktot; //Yields of photodissociation
            p_h = k_h_loss/ktot;
            p_h2 = k_h2_loss/ktot;
        }
        else{
            p_c =0.;
            p_h = 0.1;
            p_h2 = 0.;
        }
    }
    else
    {
        p_c = 0.;
        p_h = 0.;
        p_h2 = 0.;
    }
}
