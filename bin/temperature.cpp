//
//  Developed by Maria Murga
//

#include "../header/dust_evolition_pro.h"

//--------------------------------------------------------------------------
//------------------------   VIBRATION TEMPERATURE   -----------------------
//--------------------------------------------------------------------------
double devol::tvib_mol(double avib, double ncvib, double egvib, double epoint){
    if (strcmp(graintype,"hac")==0){
        int i1(nsize_heat-2),i1l(0),i1r(nsize_heat-2);
        int i2(ngap- 2),i2l(0),i2r(ngap- 2);
        int i3(nt-2),i3l(0),i3r(nt-2);
        double d3;
        double dx1,dx2,dy1,dy2,dz1,dz2;
        
        
        if(avib<=size_heat[0]){
            i1=0;
            
            i1l=0;
            i1r=0;
            dx1 = 0.5;
            dx2 = 0.5;
            
        }
        else if(avib>=size_heat[nsize_heat-1]){
            i1=nsize_heat-1;
            i1l=nsize_heat-1;
            i1r=nsize_heat-1;
            dx1 = 0.5;
            dx2 = 0.5;
        }
        else{
            
            while(!(size_heat[i1]<=avib && avib<=size_heat[i1+1])){
                
                i1=i1l+floor((i1r-i1l)/2);
                
                if(size_heat[i1]<=avib)
                    i1l=i1;
                else
                    i1r=i1;
            };
            
            i1l=i1;
            i1r=i1+1;
            dx1 = (avib-size_heat[i1])/(size_heat[i1+1]-size_heat[i1]);
            dx2 = (size_heat[i1+1]-avib)/(size_heat[i1+1]-size_heat[i1]);
        }
    
        //cout<<"avib = "<<avib<<" dx1 = "<<dx1<<" dx2 = "<<dx2<<endl;
        if(egvib<=egfull[0])
        {
            i2=0;
            
            i2l=0;
            i2r=0;
            dy1=0.5;
            dy2=0.5;
            
        }
        else if(egvib>=egfull[ngap-1])
        {
            i2=ngap-1;
            i2l=ngap-1;
            i2r=ngap-1;
            dy1=0.5;
            dy2=0.5;
        }
        else
        {
            
            while(!(egfull[i2]<=egvib && egvib<=egfull[i2+1]))
            {
                
                i2=i2l+floor((i2r-i2l)/2);
                
                if(egfull[i2]<egvib)
                    i2l=i2;
                else
                    i2r=i2;
                
            }
            
            i2l=i2;
            i2r=i2+1;
            dy1=(egvib-egfull[i2])/(egfull[i2+1]-egfull[i2]);
            dy2=(egfull[i2+1]-egvib)/(egfull[i2+1]-egfull[i2]);
        }
        //cout<<"i2 = "<<i2<<endl;
        //cout<<"egvib = "<<egvib<<" dy1 = "<<dy1<<" dy2 = "<<dy2<<endl;
        if(epoint<=emod[0])
        {
            i3=0;
            i3l = 0;
            i3r = 0;
            dz1=0.5;
            dz2=0.5;
        }
        else if(epoint>=emod[nt-1])
        {
            i3 = nt-1;
            i3l = nt-1;
            i3r = nt-1;
            dz1=0.5;
            dz2=0.5;
        }
        else
        {
            while(!(emod[i3]<=epoint && epoint<=emod[i3+1]))
            {
                
                i3=i3l+floor((i3r-i3l)/2);
                
                if(emod[i3]<=epoint)
                    i3l=i3;
                else
                    i3r=i3;
            };
            
            i3l = i3;
            i3r = i3+1;
            dz1=(epoint-emod[i3])/(emod[i3+1]-emod[i3]);
            dz2=(emod[i3+1]-epoint)/(emod[i3+1]-emod[i3]);
            //cout<<"i3 = "<<i3<<endl;
            //cout<<"epoint = "<<epoint<<" dz1 = "<<dz1<<" dz2 = "<<dz2<<endl;
        }
        //cout<<"tmod = "<<tmod[i1l][i2l][i3l]<<endl;
        return tmod[i1l][i2l][i3l] * dx2*dy2*dz2+
        tmod[i1r][i2l][i3l] * dx1*dy2*dz2+
        tmod[i1l][i2r][i3l] * dx2*dy1*dz2+
        tmod[i1r][i2r][i3l] * dx1*dy1*dz2+
        tmod[i1l][i2l][i3r] * dx2*dy2*dz1+
        tmod[i1r][i2l][i3r] * dx1*dy2*dz1+
        tmod[i1l][i2r][i3r] * dx2*dy1*dz1+
        tmod[i1r][i2r][i3r] * dx1*dy1*dz1;
    }
    else if (strcmp(graintype,"gra")==0){
        int i1(nsize_heat_pah-2),i1l(0),i1r(nsize_heat_pah-2);
        int i3(nt_heat_pah-2),i3l(0),i3r(nt_heat_pah-2);
        double d3;
        double dx1,dx2,dy1,dy2,dz1,dz2;
        if(ncvib<=nc_heat_pah[0])
        {
            i1=0;
            
            i1l=0;
            i1r=0;
            dx1 = 0.5;
            dx2 = 0.5;
            
        }
        else if(ncvib>=nc_heat_pah[nsize_heat_pah-1])
        {
            i1=nsize_heat_pah-1;
            i1l=nsize_heat_pah-1;
            i1r=nsize_heat_pah-1;
            dx1 = 0.5;
            dx2 = 0.5;
        }
        else
        {
            
            while(!(nc_heat_pah[i1]<=ncvib && ncvib<=nc_heat_pah[i1+1]))
            {
                
                i1=i1l+floor((i1r-i1l)/2);
                
                if(nc_heat_pah[i1]<=ncvib)
                i1l=i1;
                else
                i1r=i1;
            };
            
            i1l=i1;
            i1r=i1+1;
            dx1 = (ncvib-nc_heat_pah[i1])/(nc_heat_pah[i1+1]-nc_heat_pah[i1]);
            dx2 = (nc_heat_pah[i1+1]-ncvib)/(nc_heat_pah[i1+1]-nc_heat_pah[i1]);
        }
        
        if(epoint<=emod_pah[0])
        {
            i3=0;
            i3l = 0;
            i3r = 0;
            dz1=0.5;
            dz2=0.5;
        }
        else if(epoint>=emod_pah[nt_heat_pah-1])
        {
            i3 = nt_heat_pah-1;
            i3l = nt_heat_pah-1;
            i3r = nt_heat_pah-1;
            dz1=0.5;
            dz2=0.5;
        }
        else
        {
            while(!(emod_pah[i3]<=epoint && epoint<=emod_pah[i3+1]))
            {
                
                i3=i3l+floor((i3r-i3l)/2);
                
                if(emod_pah[i3]<=epoint)
                i3l=i3;
                else
                i3r=i3;
            };
            
            i3l = i3;
            i3r = i3+1;
            dz1=(epoint-emod_pah[i3])/(emod_pah[i3+1]-emod_pah[i3]);
            dz2=(emod_pah[i3+1]-epoint)/(emod_pah[i3+1]-emod_pah[i3]);
            //cout<<"i3 = "<<i3<<endl;
            //cout<<"epoint = "<<epoint<<" dz1 = "<<dz1<<" dz2 = "<<dz2<<endl;
        }
        //cout<<"tmod = "<<tmod[i1l][i2l][i3l]<<endl;
        return tmod_pah[i1l][i3l] * dx2*dz2+
        tmod_pah[i1r][i3l] * dx1*dz2+
        tmod_pah[i1l][i3r] * dx2*dz1+
        tmod_pah[i1r][i3r] * dx1*dz1;
    }
    else{
        cout<<"No such a type of dust"<<endl;
        cin.get();
        return 0.;        
    }
}

double devol::prob_mol(double ahere, double Z, double uhere, double ediss){
    
    int i1(n_adpde-2),i1l(0),i1r(n_adpde-2);
    int i2(n_udpde- 2),i2l(0),i2r(n_udpde- 2);
    int i3(n_edpde-2),i3l(0),i3r(n_edpde-2);
    double d3;
    double dx1,dx2,dy1,dy2,dz1,dz2;
    
    if(ahere<=adpde[0])
    {
        i1=0;
        
        i1l=0;
        i1r=0;
        dx1 = 0.5;
        dx2 = 0.5;
        
    }
    else if(ahere>=adpde[n_adpde-1])
    {
        i1=n_adpde-1;
        i1l=n_adpde-1;
        i1r=n_adpde-1;
        dx1 = 0.5;
        dx2 = 0.5;
    }
    else
    {
        
        while(!(adpde[i1]<=ahere && ahere<=adpde[i1+1]))
        {
            
            i1=i1l+floor((i1r-i1l)/2);
            
            if(adpde[i1]<=ahere)
                i1l=i1;
            else
                i1r=i1;
        };
        
        i1l=i1;
        i1r=i1+1;
        dx1 = (ahere-adpde[i1])/(adpde[i1+1]-adpde[i1]);
        dx2 = (adpde[i1+1]-ahere)/(adpde[i1+1]-adpde[i1]);
    }
    
    
    if(uhere<=udpde[0])
    {
        i2=0;
        i2l=0;
        i2r=0;
        dy1=0.5;
        dy2=0.5;
    }
    else if(uhere>=udpde[n_udpde-1])
    {
        i2=n_udpde-1;
        i2l=n_udpde-1;
        i2r=n_udpde-1;
        dy1=0.5;
        dy2=0.5;
    }
    else
    {
        while(!(udpde[i2]<=uhere && uhere<=udpde[i2+1]))
        {
            
            i2=i2l+floor((i2r-i2l)/2);
            
            if(udpde[i2]<uhere)
                i2l=i2;
            else
                i2r=i2;
        }
        
        i2l=i2;
        i2r=i2+1;
        dy1=(uhere-udpde[i2])/(udpde[i2+1]-udpde[i2]);
        dy2=(udpde[i2+1]-uhere)/(udpde[i2+1]-udpde[i2]);
    }
    
    if(ediss<=e_dpde[0])
    {
        i3=0;
        i3l = 0;
        i3r = 0;
        dz1=0.;
        dz2=0.;
    }
    else if(ediss>=e_dpde[n_edpde-1])
    {
        i3 = n_edpde-1;
        i3l = n_edpde-1;
        i3r = n_edpde-1;
        dz1=0.;
        dz2=0.;
    }
    else
    {
        while(!(e_dpde[i3]<=ediss && ediss<=e_dpde[i3+1]))
        {
            
            i3=i3l+floor((i3r-i3l)/2);
            
            if(e_dpde[i3]<=ediss)
                i3l=i3;
            else
                i3r=i3;
        };
        
        i3l = i3;
        i3r = i3+1;
        dz1=(ediss-e_dpde[i3])/(e_dpde[i3+1]-e_dpde[i3]);
        dz2=(e_dpde[i3+1]-ediss)/(e_dpde[i3+1]-e_dpde[i3]);
        //cout<<"i3 = "<<i3<<endl;
        //cout<<"wave_opt "<<wave_opt[i3]<<" "<<wave_opt[i3+1]<<" l = "<<l<<endl;
    }
    //cout<<"dx2 = "<<dx2<<" dy2 = "<<dy2<<" dz2 = "<<dz2;
    if (Z<=0){
        return dpde0[i1l][i2l][i3l] * dx2*dy2*dz2+
    dpde0[i1r][i2l][i3l] * dx1*dy2*dz2+
    dpde0[i1l][i2r][i3l] * dx2*dy1*dz2+
    dpde0[i1r][i2r][i3l] * dx1*dy1*dz2+
    dpde0[i1l][i2l][i3r] * dx2*dy2*dz1+
    dpde0[i1r][i2l][i3r] * dx1*dy2*dz1+
    dpde0[i1l][i2r][i3r] * dx2*dy1*dz1+
    dpde0[i1r][i2r][i3r] * dx1*dy1*dz1;
    }
    else return dpde1[i1l][i2l][i3l] * dx2*dy2*dz2+
        dpde1[i1r][i2l][i3l] * dx1*dy2*dz2+
        dpde1[i1l][i2r][i3l] * dx2*dy1*dz2+
        dpde1[i1r][i2r][i3l] * dx1*dy1*dz2+
        dpde1[i1l][i2l][i3r] * dx2*dy2*dz1+
        dpde1[i1r][i2l][i3r] * dx1*dy2*dz1+
        dpde1[i1l][i2r][i3r] * dx2*dy1*dz1+
        dpde1[i1r][i2r][i3r] * dx1*dy1*dz1;
}
