//
//  Developed by Maria Murga 
//
#include "../header/dust_evolition_pro.h"


// ----------------     Sputtering term     ----------------------------------

//          small stat sputtering
void devol::Sij(int I, int J)
{
    
    
    double Pij(0.0);
    //cout<<"graintype = "<<graintype<<endl;
    int j2=J+1;
    int i2 = I+1;
    
    double in_rate_stat_aroma, in_rate_nonstat_aroma, in_rate_stat,  in_rate_nonstat,
    in2_rate_stat_aroma, in2_rate_nonstat_aroma, in2_rate_stat,  in2_rate_nonstat,
    out_rate_stat_aroma, out_rate_nonstat_aroma, out_rate_stat,  out_rate_nonstat,
    in_bigsput_aroma_rate, in_bigsput_aroma_rate_t, in_bigsput_rate,  in_bigsput_rate_t,
    in2_bigsput_aroma_rate, in2_bigsput_aroma_rate_t, in2_bigsput_rate,  in2_bigsput_rate_t,
    out_bigsput_aroma_rate, out_bigsput_aroma_rate_t, out_bigsput_rate,  out_bigsput_rate_t;
    
    if(J < Nbin_e-1)
    {
        
        small_sput(in_rate_stat_aroma, in_rate_nonstat_aroma, in_rate_stat, in_rate_nonstat,
                   gas2dus_relative_veloc[I], T, nH,
                   eg[j2], a[I],  charge[I]);
        big_sput(in_bigsput_rate, in_bigsput_aroma_rate, a[I], gas2dus_relative_veloc[I], nH, eg[j2],  charge[I]);
        big_sput_t(in_bigsput_rate_t, in_bigsput_aroma_rate_t, a[I],  nH, T, eg[j2],  charge[I]);
    }
    small_sput(out_rate_stat_aroma, out_rate_nonstat_aroma, out_rate_stat, out_rate_nonstat,
               gas2dus_relative_veloc[I], T, nH,
               eg[J], a[I],  charge[I]);
    big_sput(out_bigsput_rate, out_bigsput_aroma_rate, a[I], gas2dus_relative_veloc[I], nH, eg[J], charge[I]);
    big_sput_t(out_bigsput_rate_t, out_bigsput_aroma_rate_t, a[I],  nH, T, eg[J],  charge[I]);
    
    if (I<Nbin_a-1)
    {
        small_sput(in2_rate_stat_aroma, in2_rate_nonstat_aroma,in2_rate_stat, in2_rate_nonstat,
                   gas2dus_relative_veloc[i2], T, nH,
                   eg[J], a[i2], charge[i2]);
        big_sput(in2_bigsput_rate, in2_bigsput_aroma_rate, a[i2], gas2dus_relative_veloc[i2], nH, eg[J],  charge[i2]);
        big_sput_t(in2_bigsput_rate_t, in2_bigsput_aroma_rate_t, a[i2],  nH, T, eg[J],charge[i2]);
    }
    if(J == Nbin_e-1)
    {
        AAA2[I][J]       = 0.0;
        AAA3[I][J] = 0.0;
        AAA4[I][J] = 0.0;
    }
    else{
        AAA2[I][J]     = in_rate_stat_aroma/(egb[j2+1] - egb[j2]);
        AAA3[I][J]     = in_rate_nonstat_aroma/(egb[j2+1] - egb[j2]);
        AAA4[I][J] = (in_bigsput_aroma_rate+in_bigsput_aroma_rate_t)/(egb[j2+1] - egb[j2]);
        /*cout<<"eg = "<<eg[j2]<<","<<egb[j2]<<endl;
         cout<<"in_rate_stat_aroma = "<<in_rate_stat_aroma<<endl;
         cout<<"in_rate_nonstat_aroma = "<<in_rate_nonstat_aroma<<endl;
         cout<<"in_bigsput_aroma_rate = "<<in_bigsput_aroma_rate<<endl;
         cout<<"in_bigsput_aroma_rate = "<<in_bigsput_aroma_rate_t<<endl;*/
    }
    if(J == 0)
    {
        AAA2_neg[I][J]       = 0.0;
        AAA3_neg[I][J]       = 0.0;
        AAA4_neg[I][J]       = 0.0;
    }
    else{
        AAA2_neg[I][J]     = - out_rate_stat_aroma/(egb[J+1]  - egb[J]);
        AAA3_neg[I][J]     = - out_rate_nonstat_aroma/(egb[J+1]  - egb[J]);
        AAA4_neg[I][J]     = - (out_bigsput_aroma_rate+out_bigsput_aroma_rate_t)/(egb[J+1]  - egb[J]);
    }
    //cout<<"I,J = "<<I<<", "<<J<<" PP1 = "<<PP1[I][J]<<" PP1_neg "<<PP1_neg[I][J]<<endl;
    //cout<<"out rate aroma "<<out_rate_stat_aroma<<endl;
    if (I<Nbin_a-1)
    {
        BBB2[I][J]     = in2_rate_stat/(mb[i2+1] - mb[i2]) ;
        BBB3[I][J] =  in2_rate_nonstat/(mb[i2+1] - mb[i2]) ;
        BBB4[I][J] = (in2_bigsput_rate+ in2_bigsput_rate_t)/(mb[i2+1] -mb[i2]);
    }
    else
    {
        BBB2[I][J] = 0.0;
        BBB3[I][J] = 0.0;
        BBB4[I][J] = 0.0;
    }
    
    BBB2_neg[I][J] = - out_rate_stat/(mb[I+1] - mb[I]);
    //cout<<"I,J = "<<I<<", "<<J<<" PP2 = "<<PP2[I][J]<<" PP2_neg "<<PP2_neg[I][J]<<endl;
    //cout<<"out rate stat "<<out_rate_stat<<endl;
    
    BBB3_neg[I][J] = -out_rate_nonstat/(mb[I+1] - mb[I]);
    BBB4_neg[I][J] = -(out_bigsput_rate + out_bigsput_rate_t)/(mb[I+1]-mb[I]);
}

void devol::small_sput(double &rate_stat_aroma, double &rate_nonstat_aroma, double &rate_stat, double &rate_nonstat, double *vpah, double T, double nH, double egold, double aa, double z_charge)
{
    double Nt_stat, Nt_nonstat;
    double rni, rni_h, rei, rei_h, nc, amed, natoms;
    
    amed = 12. - 11.*egold/4.3;
    natoms = pow(aa,3.0)*4.*M_PI*ro*_constant_NA/(3.*amed);
    
    nc = natoms *(1- egold/4.3);
    
    //  need rei for small stat sputtering
    if (nc<1000){
        i_interaction_ri(rni,   rni_h,rei,   rei_h,  'h', aa,nc,egold, nH, vpah[0], z_charge);
        Nt_nonstat      = rni  ;
        Nt_stat         = rei  ;
        rate_stat_aroma = rei_h;
        rate_nonstat_aroma = rni_h;
        //cout<<"H stat aroma "<<rate_stat_aroma<<endl;
        //cout<<"H stat inertial "<<Nt_stat<<" non-stat "<<Nt_nonstat<<endl;
        
        thermal(rni, rni_h, rei, rei_h, 'h', aa,nc, nH, T, egold,  z_charge);
        Nt_nonstat     += rni  ;
        Nt_stat        += rei  ;
        rate_stat_aroma+= rei_h;
        rate_nonstat_aroma += rni_h;
        //cout<<"H stat aroma thermal "<<rate_stat_aroma<<endl;
        //cout<<"H stat thermal "<<rni<<" non-stat "<<rei<<endl;
        
        i_interaction_ri(rni,  rni_h,rei,   rei_h ,'e', aa, nc, egold, nH, vpah[1],  z_charge);
        Nt_nonstat      += rni  ;
        Nt_stat         += rei  ;
        rate_stat_aroma += rei_h;
        rate_nonstat_aroma += rni_h;
        
        //cout<<"He stat aroma "<<rate_stat_aroma<<endl;
        //cout<<"He stat inertial "<<rni<<" non-stat "<<rei<<endl;
        
        thermal(rni, rni_h,rei, rei_h,'e', aa, nc, nH, T, egold,  z_charge);
        Nt_nonstat      += rni  ;
        Nt_stat         += rei  ;
        rate_stat_aroma += rei_h;
        rate_nonstat_aroma += rni_h;
        
        //cout<<"He stat aroma thermal"<<rate_stat_aroma<<endl;
        
        //cout<<"He stat thermal "<<rni<<" non-stat "<<rei<<endl;
        
        i_interaction_ri(rni,  rni_h,rei,   rei_h,  'c', aa, nc, egold, nH, vpah[2], z_charge);
        Nt_nonstat      += rni  ;
        Nt_stat         += rei  ;
        rate_stat_aroma += rei_h;
        rate_nonstat_aroma += rni_h;
        
        //cout<<"C stat aroma "<<rate_stat_aroma<<endl;
        //cout<<"C stat inertial "<<rni<<" non-stat "<<rei<<endl;
        
        thermal(rni,rni_h,rei, rei_h,'c', aa, nc, nH, T, egold,  z_charge);
        Nt_nonstat      += rni  ;
        Nt_stat         += rei  ;
        rate_stat_aroma += rei_h;
        rate_nonstat_aroma += rni_h;
        
        //cout<<"C stat aroma thermal"<<rate_stat_aroma<<endl;
        
        //cout<<"C stat thermal "<<rni<<" non-stat "<<rei<<endl;
        
        e_interaction_re(rei, rei_h, nc, aa, nH, T, egold,  z_charge);
        Nt_stat        += rei;
        rate_stat_aroma+= rei_h;
        
        //cout<<"electron aroma "<<rate_stat_aroma<<endl;
        
        //cout<<"nc = "<<nc<<" eg = "<<egold<<" electron thermal "<<rei<<endl;
        
        
        rate_stat_aroma *= 4.3/natoms;//aromatization rate due to collisions with ions
        
        rate_nonstat_aroma *= 4.3/natoms;
        rate_stat = amed*Nt_stat/_constant_NA;
        
        rate_nonstat =amed*Nt_nonstat*2.0/_constant_NA;
        //cout<<"aroma nonstat = "<<rate_nonstat_aroma<<" aroma stat = "<<rate_stat_aroma<<endl;
    }
    else
    {
        rate_nonstat_aroma = 0.0;
        rate_stat_aroma = 0.0;
        rate_stat = 0.0;
        rate_nonstat = 0.0;
    }
    
    
    //    cout<<rate_stat_aroma<<" "<<rate_stat<<" "<<rate_nonstat<<endl;
    //    cin.get();
}


void devol::thermal(double &rn_t, double &rn_h_t, double &re_t, double &re_h_t,
                    char iontype, double apah, double Nc0, double nH, double T, double eg,  double z_charge)
{
    double vmin, vmax;
    double fv, rn, rn_h, re, re_h, mion;
    
    
    mion = func_mion(iontype);
    
    vmin = sqrt(2.*_constant_E0/(mion*_constant_aem*6.242e11));
    vmax = 100000.e5;
    logscale(it_seems_that_strange_v,vmin, vmax, nv);
    for(int i = 0; i<nv; i++)
    {
        //it_seems_that_strange_v[i] = vmin + double(i)*(vmax - vmin)/double(nv-1);
        
        fv = 4 * M_PI * it_seems_that_strange_v[i]*it_seems_that_strange_v[i]
        * pow(0.5*mion*_constant_aem/(M_PI*_constant_boltz*T*1.6e-12),1.5)
        * exp(-0.5*mion*_constant_aem*it_seems_that_strange_v[i]*it_seems_that_strange_v[i]/(_constant_boltz*T*1.6e-12));
        
        i_interaction_ri(rn, rn_h, re, re_h, iontype, apah, Nc0, eg, nH, it_seems_that_strange_v[i],  z_charge);
        
        rn_fv[i] = rn*fv;
        rn_h_fv[i] = rn_h*fv;
        re_fv[i] = re*fv;
        re_h_fv[i] = re_h*fv;
        
    }
    
    rn_t = trapz(it_seems_that_strange_v, rn_fv, nv, nv);
    rn_h_t = trapz(it_seems_that_strange_v, rn_h_fv, nv, nv);
    re_t = trapz(it_seems_that_strange_v, re_fv, nv, nv);
    re_h_t = trapz(it_seems_that_strange_v, re_h_fv, nv, nv);
    
}



void devol::big_sput(double &bigsput_rate, double &bigsput_aroma_rate,double aa, double *vpah, double nH, double eg,  double z_charge)
{
    double amed = 12. - 11.*eg/4.3;
    double xh = eg/4.3;
    double natoms = pow(aa,3.0)*4.*M_PI*ro*_constant_NA/(3.*amed);
    double nc = natoms *(1- eg/4.3);
    
    bigsput_rate = (1-xh)*(rate_sput(aa,vpah[0],nH, 'h',eg,z_charge) +
                           rate_sput(aa,vpah[1],nH, 'e',eg, z_charge))*amed/_constant_NA;
    bigsput_aroma_rate = (xh*(rate_sput(aa,vpah[0],nH,  'h',eg,z_charge) +
                             rate_sput(aa,vpah[1],nH, 'e',eg, z_charge)))*eg/natoms;
    
}

void devol::big_sput_t(double &bigsput_rate_t, double &bigsput_aroma_rate_t,double aa, double nH, double T, double eg, double z_charge)
{
    double amed = 12. - 11.*eg/4.3;
    double xh = eg/4.3;
    double natoms = pow(aa,3.0)*4.*M_PI*ro*_constant_NA/(3.*amed);
    double nc = natoms *(1- eg/4.3);
    
    bigsput_rate_t = (1-xh)*( rsput_t('h', aa, eg, nH, T,z_charge) +
                             rsput_t('e', aa, eg, nH, T, z_charge)) * amed/_constant_NA;
    bigsput_aroma_rate_t = (xh*( rsput_t('h', aa, eg, nH, T,z_charge) +
                               rsput_t('e', aa, eg, nH, T, z_charge)))*eg/natoms;
}

double devol::rate_sput(double aa, double v, double nH, char iontype, double eg, double z_charge)
{
    
    double chi_ion, M1, E0n_ion, E0n_ion_h, L_ion, r2_ion, m2_pah, nc, amed, natoms, Fc;
    
    int z1_ion, z2_pah, zion;
    
    atomic_data(z1_ion, chi_ion, M1, E0n_ion, E0n_ion_h, L_ion, r2_ion, z2_pah, zion, m2_pah,iontype);//M1 only
    
    Fc = 1. - zion*z_charge*pow(_constant_ze,2)/_constant_e0/aa/M1/_constant_aem/(v*v); //Fc - coulombian factor
    //cout<<"Fc1 = "<<Fc<<endl;
    double E = 0.5 * M1 * _constant_aem * v * v * 6.242e11; //eV
    
    amed = 12. - 11.*eg/4.3;
    natoms = pow(aa,3.0)*4.*M_PI*ro*_constant_NA/(3.*amed);
    
    nc = natoms *(1- eg/4.3);
    
    if (nc>1000){
        return 2. * M_PI * aa * aa * v * n_ion(iontype) * Ysp( iontype, E,eg);}
    else return 0.0;
    
}

double devol::rsput_t(char iontype, double aa, double eg, double nH, double T, double z_charge)
{
    double mion = func_mion(iontype);
    double fv,E0 = 5.;
    double vmin = sqrt(2.*E0/(mion*_constant_aem*6.242e11));
    double vmax=100000.e5;
    double amed, natoms, nc;
    
    amed = 12. - 11.*eg/4.3;
    
    natoms = pow(aa,3.0)*4.*M_PI*ro*_constant_NA/(3.*amed);
    
    nc = natoms *(1- eg/4.3);
    logscale(it_seems_that_strange_v,vmin, vmax, nv);
    for (int i=0; i<nv; i++)
    {
        
        //it_seems_that_strange_v[i] = vmin+(vmax-vmin)/double(nv-1)*double(i);
        fv = 4*M_PI*pow(it_seems_that_strange_v[i],2)*pow(mion*_constant_aem/2./M_PI/_constant_boltz/T/1.6e-12,3./2.)
        *exp(-mion*_constant_aem*pow(it_seems_that_strange_v[i],2)/2./_constant_boltz/T/1.6e-12);
        
        fvxrn[i] = fv*rate_sput(aa,it_seems_that_strange_v[i],nH, iontype, eg, z_charge);
    }
    if (nc>1000){
        
        return trapz(it_seems_that_strange_v, fvxrn, nv, nv);}
    else return 0.;
}

double devol::Ysp(char iontype, double E, double eg)
{
    int z1_ion, z2_pah, zion;
    double Ysp;
    double U0,M2, Z2, K;
    double chi_ion, M1, E0n_ion, E0n_ion_h, L_ion, r2_ion, m2_pah;
    
    sputter(U0, M2, Z2, K);
    atomic_data(z1_ion, chi_ion, M1, E0n_ion, E0n_ion_h, L_ion, r2_ion, z2_pah, zion, m2_pah,iontype);
    
    double alpha = 0.3*pow(M2/M1,2./3.);
    double rpr = 1./(K*M2/M1+1);
    
    double gamma, Eth,a, e12, sn;
    
    if(M1/M2<=0)
    {
        gamma = 4.*M1*M2/pow(M1+M2,2);
        Eth = U0/(gamma*(1-gamma));
    }
    else
        Eth = 8*U0*pow(M1/M2,1./3.);
    
    a = 0.885*_constant_a_bohr/(pow(z1_ion,0.23) + pow(Z2,0.23));
    e12 = M2/(M1+M2)*a*E/(z1_ion*Z2*_constant_ze2);
    sn = 3.441*sqrt(e12)*log(e12+2.178)/(1.+6.35*sqrt(e12)+e12*(-1.708+6.882*sqrt(e12)));
    
    if (E>Eth)
        return 3.56/U0*M1/(M1+M2)*z1_ion*Z2/
        sqrt( pow(z1_ion,2./3.) + pow(Z2,2./3.) )*alpha*rpr*sn*(1. - pow(Eth/E,2.0/3.0))*pow(1. - (Eth/E),2);
    else
        return 0.;
    
}

void devol::sputter(double &U0, double &M2, double &Z2, double &K)
{
    //cout<<"graintype = "<<graintype<<endl;
    if (strcmp(graintype,"hac")==0){
        U0 = 4;
        M2 = 7.6;
        Z2 = 4;
        K = -0.04;
        
        return;
    }
    else if (strcmp(graintype,"gra")==0){
        U0 = 7.4;
        M2 = 12.;
        Z2 = 6;
        K = -0.04;
        
        return;
    }
    else if (strcmp(graintype,"sil")==0){
        U0 = 5.7;
        M2 = 23;
        Z2 = 11;
        K = 0.1;
        return;
    }
}

double devol::n_ion(char iontype)
{
    switch (iontype)
    {
        case 'h':
            
            return nH;
        case 'e':
            
            return chi_he*nH;
        case 'c':
            
            return chi_c*nH;
            
        default:
            cout<<"wrong iontype in n_ion"<<endl;
            cin.get();
            break;
    }
    return 0.;
}

double devol::fi_ion(char iontype)
{
    switch (iontype)
    {
        case 'h':
            
            return fi_h;
        case 'e':
            
            return fi_he;
        case 'c':
            
            return fi_c;
            
        default:
            cout<<"wrong iontype in fi_ion"<<endl;
            cin.get();
            break;
    }
    return 0.;
}

