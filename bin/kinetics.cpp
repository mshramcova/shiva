//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"


#ifdef CVODE
int devol::Jac(long int N, realtype t, N_Vector y, N_Vector fy, DlsMat J, void *Data,
               N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
    
    //  cout<<" in JAC"<<endl;
    int o=0;
    int o1=0;
    int o2=0;
    
    double S1, S2;
    
    for(int i2=0; i2<((devol*)Data)->Nbin_a; i2++)
    for(int j2=0; j2<((devol*)Data)->Nbin_e; j2++)
    {
        //N_VDATA(col_j) = (J->data)[o2];
        for(int i=0; i<((devol*)Data)->Nbin_a; i++)
        for(int j=0; j<((devol*)Data)->Nbin_e; j++)
        {
            
            // if (o2>0) cout<<o2<<" "<<o<<" in JAC00"<<endl;
            IJth(J,o2,o) = 0.0;
            S1=0;
            S2=0;
            
            o1=0;
            // if (o2>0) cout<<o2<<" in JAC01"<<endl;
            
            for (int i1=0; i1< ((devol*)Data)->Nbin_a; i1++)
            for (int j1=0; j1< ((devol*)Data)->Nbin_e; j1++)
            {
                
                S1 += ((devol*)Data)->CCC[i1][j1][i][j][i][j]
                * ((devol*)Data)->LLL[i1][j1][i][j] * Ith(y,o1);
                
                
                S2 += ((devol*)Data)->LLL[i1][j1][i][j] * Ith(y,o1);
                
                o1++;
                
            }
            
            IJth(J,o2,o) += S1 - S2;
            //cout<<"i = "<<i<<" j = "<<j<<" JAC after shat = "<<IJth(ydot,o)<<endl;
            if (o==o2)
            {
                IJth(J,o2,o) += ((devol*)Data)->AAA1_neg[i][j]
                + ((devol*)Data)->AAA2_neg[i][j]
                + ((devol*)Data)->BBB1_neg[i][j]
                + ((devol*)Data)->BBB3_neg[i][j]
                + ((devol*)Data)->BBB2_neg[i][j]
                + ((devol*)Data)->BBB4_neg[i][j];
                
            }
            else if((o==o2-1) & (j<(((devol*)Data)->Nbin_e-1)))
            {
                IJth(J,o2,o) += ((devol*)Data)->AAA1[i][j]
                +((devol*)Data)->AAA2[i][j]+((devol*)Data)->AAA3[i][j]+((devol*)Data)->AAA4[i][j];
            }
            else if ((o==o2-((devol*)Data)->Nbin_e) & (i<((devol*)Data)->Nbin_a-1))
            {
                IJth(J,o2,o) += ((devol*)Data)->BBB1[i][j]
                + ((devol*)Data)->BBB3[i][j]
                + ((devol*)Data)->BBB2[i][j]
                + ((devol*)Data)->BBB4[i][j];
            }
            o++;
        }
        //cout<<o2<<" in JAC1"<<endl;
        o2++;
        o = 0;
    }
    return(0);
}


int devol::RF(realtype t, N_Vector y, N_Vector ydot, void *Data)
{
    double *machEnv = new double[256];
    int o=0;
    int o1=0;
    int o2=0;
    int nn1 = 0;
    int nn2 = 0;
    double S1, S2;
    
    for(int i=0; i<((devol*)Data)->Nbin_a; i++)
    for(int j=0; j<((devol*)Data)->Nbin_e; j++)
    {
    
        Ith(ydot,o) = 0.0;
        if (((devol*)Data)->shatterdestr==1){
        S1=0;
        S2=0;
        
        o1=0;
        for (int i1=0; i1< ((devol*)Data)->Nbin_a; i1++)
        for (int j1=0; j1< ((devol*)Data)->Nbin_e; j1++)
        {
             
                o2=0;
                for (int i2=0; i2< ((devol*)Data)->Nbin_a; i2++)
                for (int j2=0; j2< ((devol*)Data)->Nbin_e; j2++)
                {
                        
                    S1 += 0.5*((devol*)Data)->CCC[i1][j1][i2][j2][i][j]
                            *((devol*)Data)->LLL[i1][j1][i2][j2] * Ith(y,o1) * Ith(y,o2);
                    o2++;

                }
            
            
            S2+=((devol*)Data)->LLL[i1][j1][i][j] * Ith(y,o1);
            
            o1++;
            
        }
            
        Ith(ydot,o) += S1 - Ith(y,o)*S2;
        }
        if(j){
            if (((devol*)Data)->photoar==1) {
                Ith(ydot,o) += ((devol*)Data)->AAA1_neg[i][j]*Ith(y,o);
        }
            if (((devol*)Data)->sputar==1) {
                Ith(ydot,o) +=((devol*)Data)->AAA2_neg[i][j]*Ith(y,o)
                            +((devol*)Data)->AAA4_neg[i][j]*Ith(y,o)
                            +((devol*)Data)->AAA3_neg[i][j]*Ith(y,o);}
        }
        if (((devol*)Data)->photodestr==1) {
            Ith(ydot,o) += ((devol*)Data)->BBB1_neg[i][j]*Ith(y,o);
            if(i<((devol*)Data)->Nbin_a-1){
                Ith(ydot,o)+= ((devol*)Data)->BBB1[i][j]*Ith(y,o+(((devol*)Data)->Nbin_e));
            }
        }
        if(j<(((devol*)Data)->Nbin_e-1)){
            if (((devol*)Data)->photoar==1){
                Ith(ydot,o)+= ((devol*)Data)->AAA1[i][j]*Ith(y,o+1);}
            if (((devol*)Data)->sputar==1) {
                Ith(ydot,o)+= ((devol*)Data)->AAA2[i][j]*Ith(y,o+1)
                            + ((devol*)Data)->AAA4[i][j]*Ith(y,o+1)
                            +((devol*)Data)->AAA3[i][j]*Ith(y,o+1);}
        }
        
        if (((devol*)Data)->sputterdestr==1){
            Ith(ydot,o) += + ((devol*)Data)->BBB3_neg[i][j]*Ith(y,o)
                        + ((devol*)Data)->BBB2_neg[i][j]*Ith(y,o)
                        + ((devol*)Data)->BBB4_neg[i][j]*Ith(y,o);

            Ith(ydot,o)+= + ((devol*)Data)->BBB3[i][j]*Ith(y,o+(((devol*)Data)->Nbin_e))
                        + ((devol*)Data)->BBB2[i][j]*Ith(y,o+(((devol*)Data)->Nbin_e))
                        + ((devol*)Data)->BBB4[i][j]*Ith(y,o+(((devol*)Data)->Nbin_e));}
        //cout<<"i,j, Ith(y), Ith(ydot) = "<<i<<" , "<<j<<" , "<<Ith(y,o)<<" , "<<Ith(ydot,o)<<endl;
        o++;
        
    }
    //cin.get();
    return(0);
}


#endif
