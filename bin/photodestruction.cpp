//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

// ----------------     Photodestruction term     ----------------------------------
void devol::Rij(int I, int J)
{
    
    if(J>0)
    {
    
        BBB1[I][J]       = 0.0;
        BBB1_neg[I][J]   = 0.0;
   
    }
    else
    {
//        double Rij(0.0);
        if (I<Nbin_a-1)
        {
        int i2=I+1;
        
        BBB1[I][J]     =  photodestruction_rate(a[i2],charge[i2], i2)/(mb[i2+1]-mb[i2]);
            //* NNN[i2][J]
        
        }
        else
        {
            BBB1[I][J] = 0.0;
        }
        BBB1_neg[I][J] = - photodestruction_rate(a[I],charge[I], I)/(mb[I+1]-mb[I]);
        //* NNN[I][J]
    }
    //cout<<"I,J = "<<I<<" "<<J<<" RRR = "<<RRR[I][J]<<" RRR_neg = "<<RRR_neg[I][J]<<endl;
}

double C2 = 6.24150934e11*_constant_hplanck*_constant_clight;
double C3  = (M_PI*M_PI) / (_constant_hplanck * _constant_clight);

double devol::photodestruction_rate(double  aa, double Z, int ihere)
{
    
    double k_h_loss, k_h2_loss, k_c2h2_loss,k_h_loss50, k_h2_loss50, k_c2h2_loss50, Y_c2h2_loss, amed, Natoms;
    int ndiss;
    ndiss = 32; //internal energies from 3 to 25 eV
    double Y_c2h2[ndiss], ediss[ndiss];
    amed = 12.;
    Natoms = pow(aa,3)*4.*M_PI*ro*_constant_NA/(3.*amed);
    //cout<<"aa = "<<aa<<"Natoms = "<<Natoms<<endl;
    Y_c2h2_loss = 0.;
    if (Natoms <2000){
        
        for (int w=0; w < ndiss; w++)
        {
            ediss[w] = 3.+ w;
            if (ediss[w]>_constant_ebind)
            {
                //Rates of disintegration of H, H2 and C2H2 according to the RRK model
                
                k_h_loss    = 1e16*pow(1.0-2.8/ediss[w],3*Natoms-7);
                k_h2_loss   = 1e16*pow(1.0-2.9/ediss[w],3*Natoms-7);
                k_c2h2_loss = 1e15*pow(1.0-2.9/ediss[w],3*Natoms-7);
                if (Natoms>50){
                    k_h_loss50    = 1e16*pow(1.0-2.8/ediss[w],3*50-7);
                    k_h2_loss50   = 1e16*pow(1.0-2.9/ediss[w],3*50-7);
                    k_c2h2_loss50 = 1e15*pow(1.0-2.9/ediss[w],3*50-7);
                    k_h_loss    = k_h_loss50*pow(10., -0.3373*(Natoms-50));
                    k_h2_loss   = k_h2_loss50*pow(10., -0.3373*(Natoms-50));
                    k_c2h2_loss = k_c2h2_loss50*pow(10., -0.3373*(Natoms-50));
                }
                    
                Y_c2h2[w] = prob_mol(aa, Z, fieldfactor, ediss[w])*k_c2h2_loss*2.;
                //cout<<"ediss, prob = "<<ediss[w]<<" , "<<prob_mol(aa, Z, fieldfactor, ediss[w])<<endl;
                //cout<<"k_c2h2_loss = "<<k_c2h2_loss<<endl;
                //cout<<"Y_c2h2_loss = "<<Y_c2h2_loss<<endl;
                Y_c2h2_loss += Y_c2h2[w];
            }
        }
        //Rate of dissociation through a loss of C2H2

        //cout<<"photorate = "<<amed/_constant_NA*Y_c2h2_loss<<endl;
        return amed/_constant_NA*Y_c2h2_loss;
    }
    else
    return 0.0;
}


double devol::kir(double aa, double nchere, double epoint, double Z)
{
    double int_func2[nlafull];
    double qabs_here, planck_here;
    double tvib_here = tvib_mol(aa, nchere, 0.1, epoint);
    //cout<<"aa = "<<aa<<" epoint = "<<epoint<<" tvib_here = "<<tvib_here<<endl;
    for (int w=0; w<nlafull; w++)
    {
        qabs_here    = optics_qabs(lafull[w], aa, 0.1, Z);
        planck_here  = planck_lambda(lafull[w], tvib_here);
        int_func2[w]  = C3 * qabs_here * aa * aa * 4. * planck_here * lafull[w];
        //cout<<"lafull = "<<lafull[w]<<" qabs_here = "<<qabs_here<<endl;
        //cout<<"planck_here = "<<planck_here<<" tvib = "<<tvib_here<<endl;
    }
    //cout<<"kir trpaz ="<<trapz(lafull, int_func, nlafull, 50)<<endl;
    return trapz(lafull, int_func2, nlafull, 50);
}

double devol::Yion(double e, double Z, double aa)
{
    
    double pet,teta_ph, Yion, Emin, W, IP, wave_di, fper, fpar, y0, y1, y2, Elow, Ehigh,alpha, la_1;
    
    W = 4.4;
    aa *= 1e8;
    IP = W + (Z+0.5)*_constant_ze2/aa + (Z+2)*_constant_ze2/pow(aa,2)*0.3;
    Emin = -(Z+1)*_constant_ze2/aa/(1. + pow(27/aa, 0.75));
    
    wave_di = _constant_hplanck_eV*_constant_clight/e*1e4;
    
    if (aa<500){
        fper = interp_1d(wave_di, wave1_dielec, imper1, n_dielec_per1);
        fpar = interp_1d(wave_di, wave3_dielec, impar1, n_dielec_par1);
    }
    else{
        fper = interp_1d(wave_di,wave2_dielec, imper2, n_dielec_per2);
        fpar = interp_1d(wave_di,wave4_dielec, impar2, n_dielec_par2);
    }
    //cout<<"fper,fpar = "<<fper<<" "<<fpar<<endl;
    la_1 = 4.*M_PI/wave_di/1e4*(2./3.*fper+1./3.*fpar); //1/A
    
    
    if (Z>=-1)
    {
        pet = IP;
    }
    else
    {
        pet = IP + Emin;
    }
    
    if (Z>=0)
    {
        teta_ph = e - pet+(Z+1)*_constant_ze2/aa;
    }
    else
    {
        teta_ph = e - pet;
    }
    
    y0 = 9e-3*pow(teta_ph/W,5)/(1. + 3.7e-2*pow(teta_ph/W,5));
    
    if (Z>=0){
        Elow = -(Z+1)*_constant_ze2/aa;
        Ehigh = e - pet;
        if (Ehigh>=0){
            y2 = pow(Ehigh,2)*(Ehigh - 3*Elow)/pow(Ehigh - Elow,3);}
        else{y2 = 0.;}
    }
    else{y2 = 1.;}
    
    alpha = aa*(_constant_le+1./la_1)/(_constant_le/la_1);
    double ksi = aa*la_1;
    y1 = pow(ksi/alpha,2)*(pow(alpha,2)-2*alpha+2-2*exp(-alpha))/(pow(ksi,2)-2*ksi+2-2*exp(-ksi));
    
    if (y0*y1<1)
    {
        Yion = y2*y0*y1;
    }
    else{
        Yion = y2;
    }
    if (Yion<0) {
        Yion=0.;
    }
    //cout<<" aa = "<<aa<<" E = "<<e<<" IP= "<<IP<<" teta = "<<teta<<" Yion= "<<Yion<<endl;
    return Yion;
    
}






