//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

#define len 100
char buf[len];
ifstream file_ind;
//--------------------------------------------------------------------------
//---------------   READ Q_ABS  --------------------------------------------
//--------------------------------------------------------------------------
void optics_readqabs(devol& obj)
{
    char fname[50];
    
    cout<<"READ Q_ABS..."<<endl;
    
    fstream F;
    
    if (strcmp(obj.graintype,"hac")==0){
        
        for (int i_eg = 0; i_eg<obj.ngap; i_eg++)
        {
            
            sprintf(fname, "ini/qabs/ach_all_%01.2f_charge.dat", obj.egfull[i_eg]);
            //cout<<"fname = "<<fname<<endl;
            F.open(fname);
            
            if (F)
            {
                F.getline(buf,len);
                F.getline(buf,len);
                F.getline(buf,len);
                F.getline(buf,len);
                F.getline(buf,len);
                
                
                for (int i=0; i<obj.nsize; i++)
                {
                    F.getline(buf,len);
                    F.getline(buf,len);
                    F.getline(buf,len);
                    
                    for (int j=0; j<obj.nwave; j++)
                    {
                        F.getline(buf,len);
                        
                        obj.wave_opt[obj.nwave -1 - j] = atof(&buf[0]);
                        obj.qabs_opt[1][i][i_eg][obj.nwave -1- j] = atof(&buf[10]);
                        //cout<<"wave,qabs = "<<obj.wave_opt[obj.nwave -1 - j]<<" "<<obj.qabs_opt[i][i_eg][obj.nwave-1- j]<<endl;
                    }
                }
#ifdef __PRTINT
                cout<<"File with optical properties of charged dust (HAC) is read "<<i_eg<<endl;
#endif
                F.close();
            }
            else
            cout<<"File with optical properties of charged dust (HAC) is not found "<<i_eg<<endl;
            
        }
        
        
        for (int i_eg = 0; i_eg<obj.ngap; i_eg++)
        {
            
            sprintf(fname, "ini/qabs/ach_all_%01.2f_corr.dat", obj.egfull[i_eg]);
            cout<<"fname = "<<fname<<endl;
            F.open(fname);
            
            if (F)
            {
                F.getline(buf,len);
                F.getline(buf,len);
                F.getline(buf,len);
                F.getline(buf,len);
                F.getline(buf,len);
                
                
                for (int i=0; i<obj.nsize; i++)
                {
                    F.getline(buf,len);
                    F.getline(buf,len);
                    F.getline(buf,len);
                    
                    for (int j=0; j<obj.nwave; j++)
                    {
                        F.getline(buf,len);
                        
                        obj.wave_opt[obj.nwave -1 - j] = atof(&buf[0]);
                        obj.qabs_opt[0][i][i_eg][obj.nwave -1- j] = atof(&buf[10]);
                        //cout<<"wave,qabs = "<<obj.wave_opt[obj.nwave -1 - j]<<" "<<obj.qabs_opt[i][i_eg][obj.nwave-1- j]<<endl;
                    }
                }
#ifdef __PRTINT
                cout<<"File with optical properties of neutral dust (HAC) is read "<<i_eg<<endl;
#endif
                F.close();
            }
            else
            cout<<"File with optical properties of neutral dust (HAC) is not found "<<i_eg<<endl;
            
        }
    }
    else if (strcmp(obj.graintype,"gra")==0){
        
        
        F.open("ini/qabs/carb+.dat");
        if (F)
        {
            F.getline(buf,len);
            F.getline(buf,len);
            F.getline(buf,len);
            F.getline(buf,len);
            F.getline(buf,len);
            
            for (int i=0; i<obj.nsize_pah; i++)
            {
                F.getline(buf,len);
                F.getline(buf,len);
                obj.size_pah[i] = atof(&buf[0])*1e-4;
                F.getline(buf,len);
                
                for (int j=0; j<obj.nwave; j++)
                {
                    F.getline(buf,len);
                    
                    obj.wave_opt[obj.nwave -1 - j] = atof(&buf[0]);
                    obj.qabs_opt_pah[1][i][obj.nwave -1- j] = atof(&buf[10]);
                }
            }
            cout<<"File with optical properties of charged dust (GRA) is read "<<endl;
            F.close();
        }
        else
        cout<<"File with optical properties of charged dust (GRA) is not found "<<endl;
        
        F.open("ini/qabs/carb0.dat");
        
        if (F)
        {
            F.getline(buf,len);
            F.getline(buf,len);
            F.getline(buf,len);
            F.getline(buf,len);
            F.getline(buf,len);
            
            for (int i=0; i<obj.nsize_pah; i++)
            {
                F.getline(buf,len);
                F.getline(buf,len);
                obj.size_pah[i] = atof(&buf[0])*1e-4;
                //cout<<"size = "<<i<<", "<<obj.size_pah[i]<<endl;
                F.getline(buf,len);
                
                for (int j=0; j<obj.nwave; j++)
                {
                    F.getline(buf,len);
                    
                    obj.wave_opt[obj.nwave -1 - j] = atof(&buf[0]);
                    obj.qabs_opt_pah[0][i][obj.nwave -1- j] = atof(&buf[10]);
                    //cout<<"wave,qabs = "<<obj.wave_opt[obj.nwave -1 - j]<<" "<<obj.qabs_opt_pah[0][i][obj.nwave-1- j]<<endl;
                }
            }
            cout<<"File with optical properties of neutral dust (GRA) is read "<<endl;
            F.close();
        }
        else
        cout<<"File with optical properties of neutral dust (GRA) is not found "<<endl;
    }
    else{
        cout<<"Optical properties for this type of grains are absent"<<endl;
    }
    cout<<"                 ... done "<<endl;
    
    
}



//--------------------------------------------------------------------------
//---------------   READ EXTERNAL DATA  ------------------------------------
//--------------------------------------------------------------------------
void read_prog_data(devol& obj)
{
    cout<<"READ EXTERNAL DATA..."<<endl;
    
    file_ind.open("ini/__program_param.ini");
    
    if(!file_ind)
    {
        cerr << "File '__program_param.ini' parameters is absent" << endl;
    }
    
    file_ind.getline(buf,len); obj.T_max = atof(&buf[0])*1000;
    file_ind.getline(buf,len); obj.dt = atof(&buf[0])*1000;
    file_ind.getline(buf,len); obj.DT = atof(&buf[0])*1000;
    file_ind.getline(buf,len); obj.dt_step = atof(&buf[0])*1000;
    file_ind.close();
    
    cout<<" Time of calculation "<<obj.T_max<<"\n Initial time step "<<obj.dt<<" \n"<<obj.DT<<" first time step"<<obj.dt_step<<endl;
    
    cout<<"                 ... done "<<endl;
    
}

//--------------------------------------------------------------------------
//---------------   READ SIZES OF ANY ARRAYS -------------------------------
//--------------------------------------------------------------------------
void read_common(devol& obj)
{
    cout<<"READ SIZES OF ANY ARRAYS..."<<endl;
    
    file_ind.open("ini/__par_arrays.ini");
    
    
    if(!file_ind)
    {
        cerr << "File '__par_arrays.ini'  is absent" << endl;
    }
    
    file_ind.getline(buf,len); obj.nsize = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nsize_pah = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.ngap = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nwave= atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_ini= atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nla = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nt = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nstep = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nlafull = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nv = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nteta = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nbin_shat = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nai = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.lenE = atoi(&buf[0]);
    
    file_ind.getline(buf,len); obj.n_ini_charge = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_ini_vel = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_dielec_per1 = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_dielec_per2 = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_dielec_par1 = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_dielec_par2 = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_ini_velgas = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_udpde = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_adpde = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.n_edpde = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nsize_heat = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nsize_heat_pah = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.nt_heat_pah = atoi(&buf[0]);
    
    file_ind.close();
    
    
    // Read file with all variable parameters
    file_ind.open("ini/__parameters.ini");
    if(!file_ind)
    {
        cerr << "File '__parameters.ini' parameters is absent" << endl;
    }
    
    file_ind.getline(buf,len); obj.Nbin_a = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.Nbin_e = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.graintype[0] = buf[0];
    obj.graintype[1] = buf[1];
    obj.graintype[2] = buf[2];
    //file_ind.getline(obj.graintype,4);
    file_ind.getline(buf,len); obj.nH = atof(&buf[0]);
    file_ind.getline(buf,len); obj.T = atof(&buf[0]);
    file_ind.getline(buf,len); obj.fieldfactor = atof(&buf[0]);
    file_ind.getline(buf,len); obj.ne = atof(&buf[0]);
    file_ind.getline(buf,len); obj.chi_he = atof(&buf[0]);
    file_ind.getline(buf,len); obj.chi_c = atof(&buf[0]);
    file_ind.getline(buf,len); obj.incharge = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.vgas = atof(&buf[0]);
    file_ind.getline(buf,len); obj.arom_init = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.fi_h = atof(&buf[0]);
    file_ind.getline(buf,len); obj.fi_he = atof(&buf[0]);
    file_ind.getline(buf,len); obj.fi_c = atof(&buf[0]);
    file_ind.getline(buf,len); obj.disp = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.gdcouple = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.vdust = atof(&buf[0]);
    file_ind.getline(buf,len); obj.sd_output = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.ts1 = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.ts2 = atoi(&buf[0]);
    
    cout<<"graintype = "<<obj.graintype<<endl;
    cout<<"the dust charge is "<<obj.incharge<<endl;
    cout<<"nH = "<<obj.nH<<" n_e = "<<obj.ne<<" chi_c = "<<obj.chi_c<<endl;
    cout<<"                 ... done "<<endl;
    file_ind.close();
    
    
    // Read file with needed processes
    file_ind.open("ini/__processes.ini");
    if(!file_ind)
    {
        cerr << "File '__processes.ini' is absent" << endl;
    }
    
    file_ind.getline(buf,len); obj.photoar = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.sputar = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.photodestr = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.sputterdestr = atoi(&buf[0]);
    file_ind.getline(buf,len); obj.shatterdestr = atoi(&buf[0]);
    
    cout<<"Processes included: "<<endl;
    if (obj.photoar==1){
        if (strcmp(obj.graintype,"hac")==0){
            cout<<"aromatisation due to photodestruction"<<endl;
        }
        else if ((strcmp(obj.graintype,"gra")==0)|(strcmp(obj.graintype,"sil")==0)){
            cout<<"aromatisation process is not applicable for this type grains and has been turned off"<<endl;
            obj.photoar=0;
        }
    }
    if (obj.sputar==1){
        if (strcmp(obj.graintype,"hac")==0){
            cout<<"aromatisation due sputtering"<<endl;}
        else if ((strcmp(obj.graintype,"gra")==0)|(strcmp(obj.graintype,"sil")==0)){
            cout<<"aromatisation process is not applicable for this type grains and has been turned off"<<endl;
            obj.sputar=0;
        }}
    if (obj.photodestr==1)
    {
        if ((strcmp(obj.graintype,"hac")==0)|(strcmp(obj.graintype,"gra")==0)){
            cout<<"photodestruction (loss of carbon atoms)"<<endl;}
        else if ((strcmp(obj.graintype,"sil")==0)){
            cout<<"photodestruction process is not applicable for this type grains and has been turned off"<<endl;
            obj.photodestr=0;
        }
    }
    if (obj.sputterdestr==1){
        cout<<"sputtering (loss of carbon atoms)"<<endl;}
    if (obj.shatterdestr==1){
        cout<<"shattering"<<endl;}
    
    file_ind.close();
    
    
    // Read file with needed processes
    file_ind.open("ini/__filenames.ini");
    if(!file_ind)
    {
        cerr << "File '__filenames.ini' is absent" << endl;
    }
    file_ind.getline(buf,len);
    //for (int i=0;i<len;i++) obj.charge_fname[i] = buf[i];
    size_t fnlen = strcspn(buf," ");
    strncpy(obj.charge_fname, buf, fnlen);
    file_ind.getline(buf,len);
    //for (int i=0;i<len;i++) obj.dmodel[i] = buf[i];
    fnlen = strcspn(buf," ");
    strncpy(obj.dmodel, buf, fnlen);
    
    if (strcmp(obj.dmodel,"MRN")==0){
        if ((strcmp(obj.graintype,"hac")==0)|(strcmp(obj.graintype,"gra")==0)) {
            strcpy(obj.distr_fname, "ini/carb_distr_mrn.dat");
        }
        else if (strcmp(obj.graintype,"sil")==0){
            strcpy(obj.distr_fname, "ini/sil_distr_mrn.dat");
        }
    }
    if (strcmp(obj.dmodel,"WD01")==0){
        if ((strcmp(obj.graintype,"hac")==0)|(strcmp(obj.graintype,"gra")==0)) {
            strcpy(obj.distr_fname, "ini/carb_distr_wd01.dat");
        }
        else if (strcmp(obj.graintype,"sil")==0){
            strcpy(obj.distr_fname, "ini/sil_distr_wd01.dat");
        }
    }
    if (strcmp(obj.dmodel,"J13")==0){
        if ((strcmp(obj.graintype,"hac")==0)|(strcmp(obj.graintype,"gra")==0)) {
            strcpy(obj.distr_fname, "ini/carb_distr_jones.dat");
        }
        else if (strcmp(obj.graintype,"sil")==0){
            strcpy(obj.distr_fname, "ini/sil_distr_jones.dat");
        }
    }
    
    file_ind.getline(buf,len,' ');
    //for (int i=0;i<len;i++) obj.flux_fname[i] = buf[i];
    fnlen = strcspn(buf," ");
    fill(obj.flux_fname,obj.flux_fname+100,'\0'); // filling flux_fname with zeros to initialize it 
    strncpy(obj.flux_fname, buf, fnlen);
    cout<<"fnlen="<<fnlen<<endl;
    file_ind.getline(buf,len);
    fnlen = strcspn(buf," ");
    strncpy(obj.turbvel_fname, buf, fnlen);
    //for (int i=0;i<len;i++) obj.turbvel_fname[i] = buf[i];
    file_ind.close();
}



//--------------------------------------------------------------------------
//------------------------   READ ARRAYS   ---------------------------------
//--------------------------------------------------------------------------

void read_allocated_arrays(devol& obj)
{
    
    file_ind.open("ini/__par_eg_full.ini");
    cout<<"eg_full "<<obj.ngap<<endl;
    if(!file_ind)
    {
        cout<<file_ind<<endl;
        cerr << "File '__par_eg_full.ini' is absent" << endl;
        cin.get();
    }
    
    file_ind.getline(buf,len);
    file_ind.getline(buf,len);
    
    for(int i=0; i<obj.ngap; i++)
    {
        file_ind.getline(buf,len);
        obj.egfull[i]= atof(&buf[0]);
        //cout<<"egfull = "<<obj.egfull[i]<<endl;
    }
    cout<<endl;
    file_ind.close();
    
    
    
    file_ind.open("ini/__par_size.ini");
    if(!file_ind)
    {
        cerr << "File '__par_size.ini' parameters is absent" << endl;
        cin.get();
    }
    
    file_ind.getline(buf,len);
    file_ind.getline(buf,len);
    
    for(int i=0; i<obj.nsize; i++)
    {
        file_ind.getline(buf,len);
        obj.size[i]= atof(&buf[0]);
    }
    cout<<endl;
    
    file_ind.close();
    
    // Reading the initial size distribution from the file
    
    file_ind.open(obj.distr_fname);
    
    if(file_ind)
    {
        for (int i = 0; i<obj.n_ini; i++)
        {
            file_ind.getline(buf,len);
            obj.a_ini[i] = atof(&buf[0]);
            obj.dnda_ini[i] = atof(&buf[12]);
            
            //cout<<obj.a_ini[i]<<obj.dnda_ini[i]<<endl;
        }
        
        file_ind.close();
        
        cout<<" File with initial size distribution is read"<<endl;
        cout<<endl;
    }
    else
    cout<<" File with initial size distribution is not found"<<endl;
    
    /*----------------     Photon wavelengths  -------------------------*/
    
    file_ind.open(obj.flux_fname);
    if(!file_ind){
        cout<<" File with ISRF kkk fluxes is not found "<<obj.flux_fname<<endl;
    }
    file_ind.getline(buf,len);
    file_ind.getline(buf,len);
    
    if(file_ind)
    {
        for (int i = 0; i<obj.nla; i++)
        {
            file_ind.getline(buf,len);
            obj.la[i] = atof(&buf[0]);
            obj.fieldflux_orig[i] = atof(&buf[10]);
            
            //cout<<"lambda, flux of ISRF: "<<obj.la[i]<<" "<<obj.fieldflux_orig[i]<<endl;
        }
        cout<<endl;
        file_ind.close();
    }
    else{
        cout<<" File with ISRF fluxes is not found "<<obj.flux_fname<<endl;
    }
    /*----------------------------------------------------------------------*/
    
    
    file_ind.open("ini/__par_ai.ini");
    
    file_ind.getline(buf,len);
    file_ind.getline(buf,len);
    if(file_ind)
    {
        for (int i = 0; i<obj.nai; i++)
        {
            file_ind.getline(buf,len);
            obj.ai[i] = atof(&buf[0]);
        }
        file_ind.close();
    }
    else
    cout<<" File __par_ai.ini is not found"<<endl;
    
    file_ind.open("ini/__par_stop_power.ini");
    
    file_ind.getline(buf,len);
    file_ind.getline(buf,len);
    
    if(file_ind)
    {
        for (int i = 0; i<obj.lenE; i++)
        {
            file_ind.getline(buf,len);
            obj.E[i] = atof(&buf[0]);
            obj.F[i] = atof(&buf[8]);
        }
        file_ind.close();
    }
    else
    cout<<" File __par_stop_power.ini is not found"<<endl;
    
    file_ind.open("ini/__par_udpde.ini");
    
    file_ind.getline(buf,len);
    
    if(file_ind)
    {
        for (int i = 0; i<obj.n_udpde; i++)
        {
            file_ind.getline(buf,len);
            obj.udpde[i] = atof(&buf[0]);
        }
        file_ind.close();
    }
    else
    cout<<" File __par_udpde.ini is not found"<<endl;
    
    
}

void read_allocated_arrays2(devol& obj)
{
    //cout<<"Charges in file "<<obj.charge_fname<<endl;
    //strcpy(obj.charge_fname,"ini/charge_wnm_U1.dat");
    cout<<"Charges in file "<<obj.charge_fname<<endl;
    file_ind.open(obj.charge_fname);
    if(file_ind)
    {
        for (int i = 0; i<obj.n_ini_charge; i++)
        {
            file_ind.getline(buf,len);
            
            obj.a_ini_charge[i] = atof(&buf[0]);
            obj.charge_ini[i] = atof(&buf[12]);
            cout<<"a,charge = "<<obj.a_ini_charge[i]<<" "<<obj.charge_ini[i]<<endl;
        }
        
        file_ind.close();
        
        cout<<" File with charges is read"<<endl;
        cout<<endl;
    }
    else
    cout<<" File with charges is not found"<<endl;
    
    file_ind.open(obj.turbvel_fname);
    if(file_ind)
    {
        for (int i = 0; i<obj.n_ini_vel; i++)
        {
            file_ind.getline(buf,len);
            
            obj.a_ini_vel[i] = atof(&buf[0]);
            obj.vel_ini[i] = atof(&buf[8]);
            obj.disp_ini[i] = atof(&buf[20]);
            cout<<"a, non-thermal vel from file= "<<obj.a_ini_vel[i]<<" "<<obj.vel_ini[i]<<endl;
        }
        
        file_ind.close();
        
        cout<<" File with non-thermal dust velocities is read"<<endl;
        cout<<endl;
    }
    else
    cout<<" File with non-thermal dust velocities is not found"<<endl;
    
    
    file_ind.open("ini/dielec_per_0.01.dat");
    
    if(file_ind)
    {
        for (int i = 0; i<obj.n_dielec_per1; i++)
        {
            file_ind.getline(buf,len);
            obj.wave1_dielec[obj.n_dielec_per1-i-1] = atof(&buf[0]);
            obj.remper1[obj.n_dielec_per1-i-1] = atof(&buf[36]);
            obj.imper1[obj.n_dielec_per1-i-1] = atof(&buf[48]);
        }
        
        file_ind.close();
        //cout<<" File 1 with dielectric permissivity is read"<<endl;
    }
    else
    cout<<" File 1 with dielectric permissivity is not found"<<endl;
    
    file_ind.open("ini/dielec_per_0.1.dat");
    
    if(file_ind)
    {
        for (int i = 0; i<obj.n_dielec_per2; i++)
        {
            file_ind.getline(buf,len);
            obj.wave2_dielec[obj.n_dielec_per2-i-1] = atof(&buf[0]);
            obj.remper2[obj.n_dielec_per2-i-1] = atof(&buf[36]);
        }
        
        file_ind.close();
        
        //cout<<" File 2 with dielectric permissivity is read"<<endl;
    }
    else
    cout<<" File 2 with dielectric permissivity is not found"<<endl;
    
    file_ind.open("ini/dielec_par_0.01.dat");
    
    if(file_ind)
    {
        for (int i = 0; i<obj.n_dielec_par1; i++)
        {
            file_ind.getline(buf,len);
            obj.wave3_dielec[obj.n_dielec_par1-i-1] = atof(&buf[0]);
            obj.rempar1[obj.n_dielec_par1-i-1] = atof(&buf[36]);
            obj.impar1[obj.n_dielec_par1-i-1] = atof(&buf[48]);
        }
        
        file_ind.close();
        
        //cout<<" File 3 with dielectric permissivity is read"<<endl;
    }
    else
    cout<<" File 3 with dielectric permissivity is not found"<<endl;
    
    file_ind.open("ini/dielec_par_0.1.dat");
    
    if(file_ind)
    {
        for (int i = 0; i<obj.n_dielec_par2; i++)
        {
            file_ind.getline(buf,len);
            obj.wave4_dielec[obj.n_dielec_par2-i-1] = atof(&buf[0]);
            obj.rempar2[obj.n_dielec_par2-i-1] = atof(&buf[36]);
            obj.impar2[obj.n_dielec_par2-i-1] = atof(&buf[48]);
        }
        
        file_ind.close();
        
        //cout<<" File 4 with dielectric permissivity is read"<<endl;
    }
    else
    cout<<" File 4 with dielectric permissivity is not found"<<endl;
    
    
}

//--------------------------------------------------------------------------
//------------------------   VIBRATION TEMPERATURE   -----------------------
//--------------------------------------------------------------------------
void read_vib_temp(devol& obj)
{
    
    cout<<"READ HEAT CAPACITY..."<<endl;
    
    fstream F;
    char fname[300];
    
    for (int i_eg=0; i_eg<obj.ngap; i_eg++)
    {
        
        sprintf(fname, "ini/heat_capacity/t_e_%01.2f.dat", obj.egfull[i_eg]);
        
        F.open(fname);
        if(F)
        {
            for (int i_size=0; i_size<8; i_size++)
            {
                F.getline(buf,len);
                obj.size_heat[i_size] = atof(&buf[6]);
                for (int i_t=0; i_t<obj.nt; i_t++)
                {
                    F.getline(buf,len);
                    obj.emod[i_t] = atof(&buf[0]);
                    obj.tmod[i_size][i_eg][i_t] = atof(&buf[9]);
                    //cout<<"emod,tmod = "<<obj.emod[i_t]<<" "<<obj.tmod[i_size][i_eg][i_t]<<endl;
                }
            }
            F.close();
        }
        else
        cout<<"File with HAC heat capacities is not found "<<i_eg<<endl;
    }
    sprintf(fname, "ini/heat_capacity/t_e_pah_cut.dat");
    F.open(fname);
    if(F)
    {
        for (int i_size=0; i_size<obj.nsize_heat_pah; i_size++)
        {
            F.getline(buf,len);
            obj.nc_heat_pah[i_size] = atof(&buf[6]);
            for (int i_t=0; i_t<obj.nt_heat_pah; i_t++)
            {
                F.getline(buf,len);
                obj.emod_pah[i_t] = atof(&buf[0]);
                obj.tmod_pah[i_size][i_t] = atof(&buf[9]);
                //cout<<"emod,tmod = "<<obj.emod_pah[i_t]<<" "<<obj.tmod_pah[i_size][i_t]<<endl;
            }
        }
        F.close();
    }
    else
    cout<<"File with PAH heat capacities is not found "<<endl;
    
    cout<<"                 ... done"<<endl;
    
    
    cout<<"READ ENERGY DISTRIBUTION"<<endl;
    for (int i_u=0; i_u<obj.n_udpde; i_u++)
    {
        sprintf(fname, "ini/dpde/Efunc_u%8.2E_neutral.dat", obj.udpde[i_u]);
        //cout<<fname<<endl;
        F.open(fname);
        if(F)
        {
            for (int i_size=0; i_size < obj.n_adpde; i_size++)
            {
                F.getline(buf,len);
                obj.adpde[i_size] = atof(&buf[0]);
                for (int i_e=0; i_e<obj.n_edpde; i_e++)
                {
                    F.getline(buf,len);
                    obj.e_dpde[i_e] = atof(&buf[0]);
                    obj.dpde0[i_size][i_u][i_e] = atof(&buf[9]);
                    //cout<<"tmod,dpdt_mod = "<<obj.t_dpdt[i_t]<<" "<<obj.dpdt0[i_size][i_u][i_t]<<endl;
                }
            }
            F.close();
            //cout<<"File with temperature distribution 0 is read "<<i_u<<endl;
        }
        else
        cout<<"File with energy distribution 0 is not found "<<i_u<<endl;
        
        sprintf(fname, "ini/dpde/Efunc_u%8.2E_ionised.dat", obj.udpde[i_u]);
        //cout<<fname<<endl;
        F.open(fname);
        if(F)
        {
            for (int i_size=0; i_size < obj.n_adpde; i_size++)
            {
                F.getline(buf,len);
                obj.adpde[i_size] = atof(&buf[0]);
                for (int i_e=0; i_e<obj.n_edpde; i_e++)
                {
                    F.getline(buf,len);
                    obj.e_dpde[i_e] = atof(&buf[0]);
                    obj.dpde1[i_size][i_u][i_e] = atof(&buf[9]);
                    //cout<<"i_u, isize, e, dpde1 = "<<i_u<<" "<<i_size<<" "<<obj.e_dpde[i_e]<<" "<<obj.dpde1[i_size][i_u][i_e]<<endl;
                }
            }
            F.close();
            //cout<<"File with temperature distribution 1 is read"<<i_u<<endl;
        }
        else
        cout<<"File with energy distribution 1 is not found "<<i_u<<endl;
    }
    cout<<"Energy distributions have been read"<<endl;
    
}



void devol::read_initial_data(void)
{
    read_prog_data(*this);
    
    read_common(*this);
    
    //------------------------------------
    //  HERE WE KNOW ALL ARRAY SIZES
    //  AND CAN ALLOCATE MEMORY
    //------------------------------------
    
    memallocate();
    
    read_allocated_arrays(*this);
    
    read_allocated_arrays2(*this);
    
    optics_readqabs(*this);
}

void devol::read_initial_data2(void)
{
    read_vib_temp(*this);
}



