//
//  Developed by Maria Murga 
//

#include "../header/dust_evolition_pro.h"

// ----------------     Aromatization term     ----------------------------------
void devol::Aij(int I, int J)
{
    
    double Aij(0.0);
    
    if(J==0)
    {
        AAA1_neg[I][J]   = 0.0;
    }
    else
    {
        AAA1_neg[I][J]   =- eg_aroma(eg[J], a[I], charge[I])/(egb[J+1]  - egb[J]);// * NNN[I][J];
    }
    if(J == Nbin_e-1)
    {
        AAA1[I][J]       = 0.0;
    }
    else
    {
        int j2=J+1;
        AAA1[I][J]       = eg_aroma(eg[j2],a[I], charge[I])/(egb[j2+1] - egb[j2]);// * NNN[I][j2];
    }
    
    //cout<<"AAA1 "<<I<<" "<<J<<" "<<AAA1[I][J]<<" "<<AAA1_neg[I][J]<<endl;
}


double devol::eg_aroma(double eg_old, double aa, double Z)
{
    
    double xhold, natoms, amed, nh, nchere, egcrit;
    egcrit= 0.1;
    double C1 = 1e-20/ pow(_constant_clight,2) / pow(_constant_hplanck,2);
    double yield = 0.1;
    if (aa>20e-8) yield = 20e-8/aa;
    if(eg_old<=egcrit)
    {
        return 0.;
    }
    else
    {
        if (aa >= 200e-8){
            aa = 200e-8;
        }
        xhold = eg_old/4.3;
        
        amed = 12. - 11.*xhold;
        natoms = pow(aa,3)*4.*M_PI*ro*_constant_NA/(3.*amed);
        
        nchere = natoms *(1- xhold);
        
        nh = natoms - nchere;
        
        for (int w = 0; w<nlafull; w++)
        {
            if ((lafull[w]>8.9e-6)&(lafull[w]<1.2e-5)){
                //int_func[w] = 1e-19*yield*optics_qabs(la[w], aa, eg_old, Z)*fieldflux[w]*5.03e7*la[w]*1e8*eg_old/natoms;
                int_func[w] = 1e-19*yield*optics_qabs(lafull[w], aa, eg_old, Z)*fieldflux[w]*5.03e7*lafull[w]*1e8*4.3;
                //cout<<"qabs = "<<optics_qabs(lafull[w], aa, eg_old, Z)<<endl;
                //cout<<"field = "<<fieldflux[w]<<endl;
            }
            else
                int_func[w] = 0.0;
            
        }
        //cout<<"integral field = "<<trapz(la, field, nla, 50)<<endl;
        //cout<<"aa = "<<aa<<" eg = "<<eg_old<<"aroma rate = "<<trapz(la, int_func, nla, 50)<<endl;
        return trapz(lafull, int_func, nlafull, 50);
    }
}
